# coding=utf-8
import math, wx

from wx.glcanvas import GLCanvas
# from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from OpenGL.WGL import *
import win32ui
import sys



sln=[]
# key : z; value : wxBitmap
images = {}
img=None
enumid = -1
def getid():
    global enumid
    enumid += 1
    return enumid

fastMoveTo=getid()
slowMoveTo=getid()
Z=getid()
rZ=getid()
rFastMove=getid()
rSlowMove=getid()
set_speed=getid()
laseron=getid()
laseroff=getid()
point_with_default_interval=getid()
point_with_interval_in_X=getid()
rSlowMove_percentage=getid()
set_speed_percentage=getid()
rZ_actual_spd=getid()


edge = [0,0,0,0,0,0]
width = 0
height = 0
depth = 0
sc = 0
xtrans = 0
ytrans = 0
ztrans = 0

show_label = False

base = None
def BuildFont ():
    global base

    wgldc = wglGetCurrentDC ()
    hDC = win32ui.CreateDCFromHandle (wgldc)


    base = glGenLists(32+96);                   # // Storage For 96 Characters, plus 32 at the start...

    # CreateFont () takes a python dictionary to specify the requested font properties. 
    font_properties = { "name" : "Courier New",
                        "width" : 0 ,
                        "height" : -16,
                        "weight" : 50
                        }
    font = win32ui.CreateFont (font_properties)
    # // Selects The Font We Want
    oldfont = hDC.SelectObject (font)
    # // Builds 96 Characters Starting At Character 32
    wglUseFontBitmaps (wgldc, 32, 96, base+32)
    # // reset the font
    hDC.SelectObject (oldfont)
    # // Delete The Font (python will cleanup font for us...)
    return

def glPrint (str):
    """ // Custom GL "Print" Routine
    """
    global base
    # // If THere's No Text Do Nothing
    if (str == None):
        return
    if (str == ""):
        return
    glPushAttrib(GL_LIST_BIT);                          # // Pushes The Display List Bits
    try:
        glListBase(base);                               # // Sets The Base Character to 32
        glCallLists(str)                                    # // Draws The Display List Text
    finally:
        glPopAttrib();                                      # // Pops The Display List Bits
    return

def draw_color_text(x,y,z,str, color = [0,1,0,0.5]):
    glColor4f(color[0],color[1],color[2],color[3])
    glRasterPos3i(x, y, z)
    glPrint(str)
    glColor3f(1,1,1)
class MyCanvasBase(GLCanvas):
    def __init__(self, parent):
        GLCanvas.__init__(self, parent, -1)
        self.init = False
        # initial mouse position
        self.lastx = self.x = 30
        self.lasty = self.y = 30
        self.size = None
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.OnEraseBackground)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.Bind(wx.EVT_CHAR, self.OnChar)
        self.lastxr = self.xr = 30
        self.lastyr = self.yr = 30

    def OnEraseBackground(self, event):
        pass # Do nothing, to avoid flashing on MSW.


    def OnSize(self, event):
        size = self.size = self.GetClientSize()
        if self.GetContext():
            self.SetCurrent()
            glViewport(0, 0, size.width, size.height)
        event.Skip()


    def OnPaint(self, event):
        dc = wx.PaintDC(self)
        self.SetCurrent()
        if not self.init:
            self.InitGL()
            self.init = True
        self.OnDraw()


    def OnMouseDown(self, evt):
        self.CaptureMouse()
        self.x, self.y = self.lastx, self.lasty = evt.GetPosition()

    def OnChar(self, evt):
        keycode = evt.GetKeyCode()
        step = 0.1
        if keycode == 315:
            glTranslatef(0, step, 0)
        elif keycode == 316:#right
            glTranslatef(step, 0, 0)
        elif keycode == 314:#left
            glTranslatef(-step,0, 0)
        elif keycode == 317:#down
            glTranslatef(0,-step, 0)
        elif keycode == 108:
            global show_label
            show_label = not show_label
        self.Refresh(False)

    def OnMouseUp(self, evt):
        self.ReleaseMouse()
    def OnMouseMotion(self, evt):
        if evt.Dragging() and evt.LeftIsDown():
            self.lastx, self.lasty = self.x, self.y
            self.x, self.y = evt.GetPosition()
            self.Refresh(False)
        if evt.Dragging() and evt.RightIsDown():
            self.lastxr, self.lastyr = self.xr, self.yr
            self.xr, self.yr = evt.GetPosition()
            self.Refresh(False)



class CubeCanvas(MyCanvasBase):
    def InitGL(self):
        glClearColor(0.1, 0.1, 0.1, 0.0)
        # set viewing projection
        glMatrixMode(GL_PROJECTION)
        glFrustum(-0.5, 0.5, -0.5, 0.5, 1.0, 3.0)

        # position viewer
        glMatrixMode(GL_MODELVIEW)
        # glTranslatef(0.0, 0.0, -2.0)
        glTranslatef(xtrans, ytrans, ztrans-2.0)

        # position object
        glRotatef(20, 1.0, 0.0, 0.0)
        # glRotatef(self.x, 0.0, 1.0, 0.0)

        glEnable(GL_DEPTH_TEST)
        # glEnable(GL_LIGHTING)
        # glEnable(GL_LIGHT0)
        BuildFont ()

    def OnDraw(self):
        # clear color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glPushMatrix()
        draw_solution(sln)
        glPopMatrix()


        if self.size is None:
            self.size = self.GetClientSize()
        w, h = self.size

        # glColor4f(1, 0, 0, 0.5)
        # glRasterPos3f(-0.8, 0, 0)
        # glPrint("l: to show label")
        # glColor3f(1,1,1)

        w = max(w, 1.0)
        h = max(h, 1.0)
        w,h = 50,50
        
        xScale = 0.1
        yScale = 0.1
        # glLoadIdentity()
        # glRotatef((self.y - self.lasty) * yScale, 1.0, 0.0, 0.0);
        glRotatef((self.x - self.lastx) * xScale, 0.0, 1.0, 0.0);

        self.SwapBuffers()

def draw_solution(sln):
    vx=0
    vy=0
    vz=0
    global sc
    glScalef(1.0/sc, 1.0/sc, 1.0/sc)

    sss = "<- start corner (%ld,%ld,%ld)" % (edge[0], edge[2], edge[4])
    eee = "<- end corner (%ld,%ld,%ld)" % (edge[1], edge[3], edge[5])
    draw_color_text(edge[0], edge[2], edge[4], sss, color = [1,0,0,0.5])
    draw_color_text(edge[1], edge[3], edge[5],eee, color = [1,0,0,0.5])
    # print xtrans,ytrans,ztrans
    for a in range(len(sln)) :
        flag=sln[a][2]
        global fastMoveTo
        if flag==fastMoveTo:
            vx=sln[a][0]
            vy=sln[a][1]
            if show_label:
                draw_color_text(vx, vy, vz,"fastmoveto(%ld,%ld,%ld)" % (vx, vy, vz))
            continue
        if flag==slowMoveTo:#慢速移动到坐标
            vx=sln[a][0]
            vy=sln[a][1]
            glVertex3i(vx, vy, vz)
            continue
        if flag==point_with_default_interval or flag == point_with_interval_in_X:
            glBegin(GL_POINTS)
            glVertex3i(vx, vy, vz)
            glEnd()
            if show_label:
                draw_color_text(vx, vy, vz,"p(%ld,%ld,%ld)" % (vx, vy, vz))
            continue
        if flag==rZ or flag == rZ_actual_spd:
            vz += sln[a][0]
            glVertex3i(vx, vy, vz)
            continue
        if flag==rFastMove:
            vx+=sln[a][0]
            vy+=sln[a][1]
            continue
        if flag==rSlowMove  or flag == rSlowMove_percentage:
            vx+=sln[a][0]
            vy+=sln[a][1]
            glVertex3i(vx, vy, vz)
            continue
        if flag==Z:
            vz = sln[a][0]
            if show_label:
                draw_color_text(vx, vy, vz,"z %ld" % (vz))
            continue
        if flag==laseron:
            glBegin(GL_LINE_STRIP)
            glVertex3i(vx, vy, vz)
            continue
        if flag==laseroff:
            glEnd()
            continue
        if flag==set_speed or flag == set_speed_percentage:
            continue
        print "UNknown command:", flag

def get_solution_info(rpath):
    import os
    pipe = os.popen('%s\\solution_info.exe %s\\sln.txt' % (rpath,rpath))
    infostr = pipe.readline()
    pipe.close()
    slninfo_str = infostr.split(' ')
    print slninfo_str
    assert(len(slninfo_str) == 8)

    global edge
    for x in xrange(1,6):
        edge[x] = int(slninfo_str[x+2])

def dofile(filepath):
    slnfile=open(filepath,'r')
    print 'This is a tool Written by dbian,used to parser the current solution file,enjoy it'
    print 'HEADER:',slnfile.readline(),
    cfg=slnfile.readline().split(';')
    print 'speed(%):',cfg[0]
    print 'shutter interval(ms):',cfg[1]
    line=None
    while(True):
        line=slnfile.readline()
        if len(line)!=0:
            splitline=line.split(';')
            global sln
            sln.append([int(splitline[0]),int(splitline[1]),int(splitline[2])])
        else:
            break
        #global sln        
    print 'command number: %d' % len(sln)
if __name__ == '__main__':
    rpath = "."
    if len(sys.argv) > 1:
        rpath = sys.argv[1]
    dofile('%s\\sln.txt' % rpath)
    get_solution_info(rpath)
    width = edge[1]-edge[0]
    height = edge[3]-edge[2]
    depth = edge[5]-edge[4]
    assert(width>=0 and height >=0 and depth>=0)
    sc = max(max(width, height), depth)
    centrex = edge[0] + width/2
    centrey = edge[2] + height/2
    centrez = edge[4] + depth/2
    xtrans = 0
    ytrans = 0
    ztrans = 0
    if not width == 0:
        xtrans = -centrex/(1.0*width)
    if not height == 0:
        ytrans = -centrey/(1.0*height)
    if not depth == 0:
        ztrans = -centrez/(1.0*depth)

    app = wx.App()
    frame = wx.Frame(None, -1, 'Solution Previewer', wx.DefaultPosition, wx.Size(1000,700))
    canvas = CubeCanvas(frame)
    frame.Centre()
    frame.Show()
    app.MainLoop()