# -*- mode: python -*-
a = Analysis(['sln_gl_frontend.py'],
             pathex=['E:\\wk_\\cpp\\wjg\\tools'],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=os.path.join('dist', 'sln_gl_frontend.exe'),
          debug=False,
          strip=None,
          upx=True,
          console=False , icon='..\\wjg\\FSLA.ico')
app = BUNDLE(exe,
             name=os.path.join('dist', 'sln_gl_frontend.exe.app'))
