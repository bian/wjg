﻿#pragma once

#include <vector>
#include <set>
#include <map>
using namespace std;

//代表一个点
class Pt
{
public:
	Pt(){x=-1;y=-1;};
	Pt(long x_,long y_){x=x_;y=y_;};
	~Pt(){}
//	Pt(short x_,short y_){x=x_;y=y_;};
	bool operator<(const Pt &a) const
	{
		if(y<a.y)
			return true;
		else if(y==a.y)
			return x<a.x;
		else
			return false;
	};

	long x;
	long y;
};
typedef  set<Pt> spic;//代表一张图片里的有效点的集合

void getsolution(const set<Pt>& pic_,vector<slnn>& sln,int pixelMagnitude, int global_spd, int pi)
{
    spic pic;
    pic=pic_;
    long kx=-1,ky=0;
    bool Line=false,Turn = true;
    int kn,mkn=-1;
    sln.clear();

    while(!(pic.empty()))
    {
        /*允许斜边相连
        >>选择优先级与命名
        * 4 0 6
        * 2   3
        * 5 1 7
        */
        if(!Line){
            kx=pic.begin()->x;
            ky=pic.begin()->y;
        }

        auto endflag=pic.end();

        kn=-1;

        if(pic.find(Pt(kx,ky-1))!=endflag)
            kn=0;
        else if(pic.find(Pt(kx,ky+1))!=endflag)
            kn=1;
        else if(pic.find(Pt(kx-1,ky))!=endflag)
            kn=2;
        else if(pic.find(Pt(kx+1,ky))!=endflag)
            kn=3;
        else if(pic.find(Pt(kx-1,ky-1))!=endflag)
            kn=4;
        else if(pic.find(Pt(kx-1,ky+1))!=endflag)
            kn=5;
        else if(pic.find(Pt(kx+1,ky-1))!=endflag)
            kn=6;
        else if(pic.find(Pt(kx+1,ky+1))!=endflag)
            kn=7;

        //printf("cur kn is %d\n", kn);
        //flag  可用状态:1,3;10,2;
        slnn sn;
        int spd=100;
        auto push_spd_setting = [&sln, &spd](int spd_) -> void
        {
            slnn spd_sln;
            spd_sln.set(spd_, 0, SetSpeed_p);
            sln.push_back(spd_sln);
            spd = 100;
        };

                //if(spd!= 100 && (--sln.end() )->l == MoveXY_p )

        auto match_head = [&sln]()->bool
        {
            if(sln.size()<2)
                return false;
            auto last= --sln.end();
            return last->l == LaserOn && \
                (--last)->l == MoveXY_p;
        };

        auto match_node = [&sln]()->bool
        {
            if(sln.size()<2)
                return false;
            auto last= --sln.end();
            return last->l == MoveXY_p;
        };

        int state = 0;//0 nothing, 1 open, 2 close, 3 point
        if(kn!=-1)//该点周围有点
        {
            if(!Line)
            {
                sn.l=MoveXY_p;
                push_spd_setting(100);
                state = 1;
            }
            else
            {
                if(match_head())
                {
                    push_spd_setting(global_spd);
                }
                else if(match_node())
                {
                    if(!Turn)
                    {
                        sln.pop_back();
                    }
                }
                else
                {
                    throw "match failed!";
                }
                Turn=(mkn!=kn);//已修正，判断下一个点的生存
                sn.l=MoveXY_p;
            }
            Line=true;//有线头
        }
        else//孤点。可能之前有相邻点，但被擦除了，需要看是否在画线
        {
            if(!Line)//未画线
            {
                sn.l=MoveXY_p;
                push_spd_setting(100);
                state = 3;
            }
            else//这个点是线的尾巴
            {
                if(match_head())
                {
                    push_spd_setting(global_spd);
                }
                else if(match_node())
                {
                    if(!Turn)
                    {
                        sln.pop_back();
                    }
                }
                sn.l=MoveXY_p;
                state = 2;

                Line=false;//一条线路完成了
            }
        }
        //将点保存到方案
        sn.x=kx*pixelMagnitude;
        sn.y=ky*pixelMagnitude;
        sln.push_back(sn);
        if (state == 1)
            sln.push_back(slnn(0,0,LaserOn));
        else if (state ==2)
            sln.push_back(slnn(0,0,LaserOff));
        else if(state == 3)
            sln.push_back(slnn(pi,0,Point));

        //erase
        pic.erase(Pt(kx,ky));

        //next
        switch(kn){
        case 0: ky-=1;break;
        case 1: ky+=1;break;
        case 2: kx-=1;break;
        case 3: kx+=1;break;
        case 4: kx-=1;ky-=1;break;
        case 5: kx-=1;ky+=1;break;
        case 6: kx+=1;ky-=1;break;
        case 7: kx+=1;ky+=1;
        }
        mkn=kn;
    }
}
//包含一个平面和一个解决方案
struct piece
{
	piece()
	{
		init();
	}
	~piece()
	{
		mimg.clear();
		sln.clear();
	}
	piece(long xl,long yl)
	{
		init();
		mimg.insert(Pt(xl,yl));
	}
	spic mimg;//有效点
	vector<slnn> sln;//方案

	inline void Add(long x1,long y1)
	{
		mimg.insert(Pt(x1,y1));
	}
	inline void Process(int pixelMagnitude, int spd, int pi)
	{
		getsolution(mimg,sln,pixelMagnitude, spd, pi);
	}
	inline void init()
	{
		sln.reserve(200000);
		//status=IDLE;
		mimg.clear();
		sln.clear();
	}
};
typedef  map<long,piece> pieces;//前者代表z轴


