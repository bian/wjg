﻿#include <iostream>
#include <fstream>
#include <functional>
#include <algorithm>

#include "wx/wxprec.h"
#include "wx/image.h"
#include <wx/filename.h>

#include <windows.h>
#include "def.h"
#include "m.h"
using namespace std;

vector<slnn>sln;
piece sbmp;//作为图像处理用
pieces chain;//3d

int g_spd;
int g_pi;

//declaration
bool genBmpSln(const wchar_t*path,long dia);
bool gen3DSln(BSTR path,long dia);
void writeSlnToFile(const vector<slnn>&which);
void add3DCord(long x,long y,long z);

const char flag[]="weijg sln v4";
char**args;

//-- 直径；打点间隔；扫线间隔
int main(int argc, char* argv[])
{
    ::setlocale(LC_ALL,"chs");
    if(argc<5)
    {
        cout<<"缺少参数"<<endl;
        system("pause");
        return 0;
    }
	args=argv;
	wchar_t tmp[200];
	mbstowcs(tmp,argv[1],100);
    wxString path(tmp);
    long diameter=atol(argv[2]);

    g_spd = atoi(argv[3]);
    g_pi = atoi(argv[4]);

    wxFileName fname;
    fname.Assign(path);
    if(fname.GetExt()==L"skp")
    {
        BSTR ppath = ::SysAllocString(tmp);
        gen3DSln(ppath,diameter);
    }
    else
    {
      wxInitAllImageHandlers();
      genBmpSln(tmp,diameter);
    }
    return 0;
}


//implemetation
bool genBmpSln(const wchar_t*path,long dia)
{
    wxImage isrc;
    if(isrc.LoadFile(path))
    {
        int iw=isrc.GetWidth();
        int iH=isrc.GetHeight();
        cout<<" Pixels: "<<iw<<" X "<<iH;
		cout.flush();
        //初始化数据
        sbmp.init();

        //取点
        for(int i=0; i<iw; i++) //行
        {
            for(int j=0; j<iH; j++) //列
            {
                if(isrc.GetBlue(i,j)>=240&&isrc.GetRed(i,j)>=240&&\
                        isrc.GetGreen(i,j)>=240)
                {
                    sbmp.Add(i,j);
                }
            }
        }
        cout<<" 设置单个像素的加工直径为: "<<dia;
		cout.flush();
        sbmp.Process(dia, g_spd, g_pi);
        cout<<" 缓存数据到sln.txt";
		cout.flush();
        writeSlnToFile(sbmp.sln);
    }
    else
        cout<<" 图片无效."<<endl;
    return true;
}

bool gen3DSln(BSTR path,long dia)
{
    typedef void (*add_cord)(long,long,long);
    typedef void (*push_line)(long,long,long,long,long,long);
    typedef void (*GetData)(BSTR, add_cord,long*) ;
    HMODULE pd3=::LoadLibraryA("D3Convert.dll");
    if(!pd3)
    {
        cout<<"Can not load D3Convert.dll.";
		cout.flush();
        return false;
    }
    GetData get3Ddata = (GetData)GetProcAddress(pd3,"CreateD3Points");

    long ext[3]= {0,0,0};
    cout<<"提取模型坐标，生成缩略图...";
	cout.flush();
    get3Ddata(path,add3DCord,ext);
    cout<<"Unloading 3d library";
	cout.flush();
    ::FreeLibrary(pd3);
    SysFreeString(path);

    cout<<"生成加工指令..."<<endl;

    long size=chain.size();
    long j=0;

    sln.clear();
	for(auto i = chain.begin(); i!=chain.end(); i++)
    {
        int prog = (100*(j++ +1)/size);
        if(prog%10 == 0)
            cout<<"已处理 "<<prog<<"%%"<<endl;
        i->second.Process(dia, g_spd, g_pi);
        //add z
        slnn command(i->first*dia,12,Z_p);
        sln.push_back(command);
        //copy
		for_each(i->second.sln.begin(), i->second.sln.end(), [](slnn& kk){
            sln.push_back(kk);
		});
    }
    cout<<" 缓存数据..";
	cout.flush();
    writeSlnToFile(sln);
    return true;
}

//
void add3DCord(long x,long y,long z)
{
    auto iter=chain.end();
    auto fiter=chain.find(z);
    if(fiter==iter)
        chain.insert(pair<long,piece>(z,piece(x,y)));
    else
        fiter->second.Add(x,y);

}

void writeSlnToFile(const vector<slnn>&which)
{
    //streambuf*backup=cout.rdbuf();
    fstream  ofs;
    ofs.open("sln.txt",ios_base::out | ios_base::trunc);
    if (!ofs.bad())
    {
    	ofs<<flag<<endl;

        for(size_t y=0; y!=which.size(); y++)
        {
            ofs<<which[y].x<<";"<<which[y].y<<";"<<which[y].l<<endl;
        }
        ofs.close();
    }
}

