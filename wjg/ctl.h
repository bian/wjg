﻿#pragma once
#include "logger.hpp"

#include "ctlInterface.h"


class viewInterface;
class deviceInterface;
class controller : public ctlInterface
{
public:
	controller(viewInterface*main_app);
	~controller();



	void StopStageImmediately();
	bool ReOpenStagePort(int p = -1);
	bool ReOpenShutterPort(int p=-1);
	void SetStageCtlShutter(bool b);
	void FetchStageCoordinates();
	void ProcessSolution(const Solution& sln);

	void StartMachining();
	void SetXYCord0();
	void SetCord(int axis, long cord_in_um);
	bool ShutterOpen();
	bool ShutterClose();
	bool ShutterOpenWithTimeInterval(int ms);

	void StartLuaCompileThread(std::map<int,int>& pp);
	void StartGenerateSolutionForImportedFilesThread(const char*file_path,
		int diameter,
		int percent_spd,
		int interval);

	void MoveAxisXY(long xxx, long yyy, int sp, bool laser, bool speed_mode);
	void MoveAxisZ(long zzz, int sp, bool laser, bool speed_mode);


private:
	deviceInterface* dev;
	config config_;
	viewInterface* view;

	void InitDevices();
	void InitLuaEngine();

	bool moveR(long xr,long yr,int speed);
	bool moveZR(long zr,int speed);
	void ParserSingleCommand(const slnn& single_cmd);

	void LuaCompileFunc(boost::shared_ptr<thread_paras> paras, std::map<int,int>&pp);
	void GenSlnForImportedFilesFunc(boost::shared_ptr<thread_paras> paras,
	const char*file_path, int diameter, int percent_spd, int interval);
};

//int add(lua_State* l);

extern Solution g_solution;

int add_interface(lua_State* l);

