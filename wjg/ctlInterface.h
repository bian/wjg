﻿#pragma once

#include <fstream>
#include "vector"
#include "map"
#include "string"

#include "lua_engine.h"
#include "def.h"

#include "configs.h"

using namespace std;

struct slnn;

class ctlInterface
{
public:
	ctlInterface(){};
	virtual ~ctlInterface(){};

	struct luaPlugin
	{
		luaPlugin(){}
		luaPlugin(const char*p)
			:filepath(p)
		{}
		~luaPlugin(){}
		string filepath;
		map<int,string>key;
		map<int,int>value;
	};

	class thread_paras
	{
	public:
		thread_paras(){
			bexit = false;
			thread_first = new boost::mutex;
			thread_first->lock();
			wait_ui = new boost::mutex;
			wait_ui->lock();
		}
		~thread_paras(){
			delete wait_ui;
			delete thread_first;
		}
		bool bexit;
		std::queue<wxString> msg_;

		boost::mutex* thread_first;
		boost::mutex* wait_ui;
	};

	virtual bool ReOpenStagePort(int p = -1) = 0;
	virtual bool ReOpenShutterPort(int p=-1) = 0;
	virtual void StopStageImmediately() = 0;

	virtual  void ParserSingleCommand(const slnn& single_cmd)=0;
	virtual bool moveR(long xr,long yr,int speed)=0;
	virtual bool moveZR(long zr,int speed)=0;
	/*virtual int get_stage_port_number()=0;
	virtual int get_shutter_port_number()=0;
	virtual void set_shutter_port_number(int p)=0;
	virtual void set_stage_port_number(int p)=0;
	virtual bool is_stage_ctl_shutter()=0;
	virtual void set_stage_ctl_shutter(bool b)=0;*/

	virtual void SetXYCord0()=0;
	virtual void SetCord(int axis, long cord_in_um) = 0;
	virtual bool ShutterOpen() = 0;
	virtual bool ShutterClose() = 0;
	virtual bool ShutterOpenWithTimeInterval(int ms) = 0;

	virtual void StartMachining() = 0;
	virtual void FetchStageCoordinates()=0;
	virtual void ProcessSolution(const Solution& sln)=0;

	virtual void StartLuaCompileThread(std::map<int,int>& pp)=0;
	virtual void StartGenerateSolutionForImportedFilesThread(const char*file_path,
		int diameter,
		int percent_spd,
		int interval)=0;

	virtual void MoveAxisXY(long xxx, long yyy, int sp, bool laser, bool speed_mode)=0;
	virtual void MoveAxisZ(long zzz, int sp, bool laser, bool speed_mode)=0;



	boost::mutex mu;
	boost::condition_variable waker;
	enum FLAG
	{
		f_running=0,
		f_pause,
		f_stop
	};
	int flag;
	lua_engine lua;

	map<int,luaPlugin>luaPlugins;

	config& GetConfig(){
		return config_;
	}

private:
	config config_;
	/*int basexy, basez, basevxy, basevz;
	bool right_is_positive, up_is_positive, zup_is_positive;
	bool exchange_z;
	bool b_hotfix;*/
};
extern ctlInterface* ctl;
