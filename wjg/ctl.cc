﻿/**
* controller
*/
#include "const.h"

#include "ctl.h"
#include "viewInterface.h"
#include "devices.h"
#include "ago.h"
#include "logger.hpp"
//#include <functional>

logger lg;
Solution g_solution;
ctlInterface* ctl;

fstream* fst;


controller::controller(viewInterface*main_app)
	:dev(new devices()), view(main_app)
{
	InitDevices();
	InitLuaEngine();


}

controller::~controller(){
}

void controller::InitDevices(){
	if(!dev->setup_module()) 
	{
		msg(L"module missing!");
		return;
	}
	dev->SetupShutterInterfaces(config_.is_stage_ctl_shutter());


}

void controller::InitLuaEngine(){
	static luaL_Reg funcs[] =
	{
		{"add", add_interface},
		{NULL, NULL}
	};
	lua.RegFunc(funcs);
}

void controller::StopStageImmediately(){
	dev->stage.stop();
}

bool controller::ReOpenStagePort(int p ){
	if(p==-1)
		p = config_.get_stage_port_number();
	else
		config_.set_stage_port_number(p);

	bool res = dev->stage.openCom(p);
	lg << "Opening stage port: "<<(res?"succeeded":"failed")<<endl;

	view->SetPortConnected(true, res);
	if(res)
		boost::thread fetch_stage(bind(&controller::FetchStageCoordinates, this));
	return res;
}

bool controller::ReOpenShutterPort(int p){
	if(p==-1)
		p = config_.get_shutter_port_number();
	else
		config_.set_shutter_port_number(p);

	bool res = dev->shutter.openCom(p);
	lg << "Opening shut port: "<<(res?"succeeded":"failed")<<endl;

	view->SetPortConnected(false, res);
	return res;

}

void controller::FetchStageCoordinates()
{
	lg<<"Started fetching-cord-thread.."<<endl;
	if(dev->stage.is_open()){
		dev->stage.init_stage();
		lg<<"Initializing stage."<<endl;
		long x,y,z;
		wxString statusbarMsg;
		wxString cc[]={L"[=  ]",L"[ = ]",L"[  =]",L"[ = ]"};
		unsigned int ii = 0;
		while(dev->stage.is_open())
		{
			boost::mutex::scoped_lock slock(ctl->mu);
			if(dev->stage.getcurpos(x,y,z))
				statusbarMsg.Printf(L" :%0.2f,%0.2f,%0.2f",
				x/(config_.get_basexy()+0.0),y/(config_.get_basexy()+0.0),z/(config_.get_basez()+0.0));
			else
				statusbarMsg=L" Retrying...";
			slock.unlock();
			while(ii<4){
				view->SetStatusbarText(cc[ii++] + statusbarMsg,1);
				Sleep(400);
			}
			ii = 0;
		}
	}
	view->SetStatusbarText(L"Failed to connect stage port!",1);
	lg<<"Fetching cord thread exited.."<<endl;
}


void controller::SetXYCord0(){
	dev->stage.px(0);
	dev->stage.py(0);
}

void controller::SetCord(int axis, long cord_in_um){
	switch(axis){
	case 0:
		dev->stage.px(cord_in_um);
		break;
	case 1:
		dev->stage.py(cord_in_um);
		break;
	case 2:
		dev->stage.pz(cord_in_um);
		break;
	}
}

bool controller::ShutterOpen(){
	return dev->ShutterOpen();
}
bool controller::ShutterClose(){
	return dev->ShutterClose();
}
bool controller::ShutterOpenWithTimeInterval(int ms){
	return dev->ShutterOpenWithTimeInterval(ms);
}

void controller::StartMachining(){
	auto pview = &(view);
	boost::thread wt([pview]()
	{
		(*pview)->Log(L"加载方案...");
		if(! parseSolution(g_solution)) {
			(*pview)->Log(L"方案存在错误.");
			return;
		}
		boost::mutex::scoped_lock slock(ctl->mu);
		(*pview)->Log(L"加工中...");
		ctl->ProcessSolution(g_solution);
		(*pview)->Log(L"加工结束");
	});

}
void controller::GenSlnForImportedFilesFunc(boost::shared_ptr<thread_paras> paras,
	const char*file_path, int diameter, int percent_spd, int interval)
{
	paras->thread_first->unlock();
	
	paras->wait_ui->lock();

	HANDLE rd,wt;
	SECURITY_ATTRIBUTES sa;
	sa.bInheritHandle=TRUE;// read and write access
	sa.lpSecurityDescriptor=NULL;
	sa.nLength=sizeof(sa);
	if(!::CreatePipe(&rd,&wt,&sa,1000))
	{
		paras->msg_.push(L"创建管道失败");
		throw GetLastError();
	}
	STARTUPINFOW si;
	memset(&si,0,sizeof(si));
	si.wShowWindow=SW_HIDE;
	si.dwFlags=STARTF_USESHOWWINDOW|STARTF_USESTDHANDLES;
	si.cb=sizeof(si);
	si.hStdOutput=wt;
	si.hStdError=wt;
	PROCESS_INFORMATION pi;
	memset(&pi,0,sizeof(pi));
	wxString cmdstr;
	cmdstr.Printf(L"weijgcs \"%s\" %d %d %d", file_path, diameter, percent_spd, interval);
	paras->msg_.push(L"创建解析进程");
	BOOL res=::CreateProcessW(NULL,cmdstr.wchar_str(), NULL,NULL,TRUE,0,NULL,NULL,&si,&pi);
	if(!res)
	{
		paras->msg_.push(L"Create progress failed!");
		throw GetLastError();
	}
	char buf[1024];
	DWORD readBytes;
	paras->msg_.push(L"读取IPC管道..");
	CloseHandle(wt);// should close first, otherwise will block reading
	while(true)
	{
		memset(buf,0,1024);
		res=ReadFile(rd,buf,1024,&readBytes,NULL);
		if(res==TRUE && readBytes!=0)
		{
			paras->msg_.push(wxString(buf, wxConvUTF8));
		}
		else
			break;
	}
	CloseHandle(rd);
	paras->msg_.push(L"方案已生成.\n");
	paras->bexit = true;

}

void controller::StartGenerateSolutionForImportedFilesThread(const char*file_path, int diameter, int percent_spd, int interval)
{
	boost::shared_ptr<thread_paras> paras(new thread_paras());
	boost::thread th_real_worker(boost::bind(&controller::GenSlnForImportedFilesFunc, this,
		paras,
		file_path, diameter, percent_spd, interval
		));

	paras->thread_first->lock();
	view->ShowProgressDlg(paras);
	
}
void controller::LuaCompileFunc(boost::shared_ptr<thread_paras> paras, std::map<int,int>&pp){
	fst= new fstream();
	fst->open("sln.txt",ios_base::out | ios_base::trunc);//override
	*(fst)<<"weijg sln v4"<<endl;

	paras->thread_first->unlock();
	
	paras->wait_ui->lock();

	paras->msg_.push(L"Pushing data..");
	lua.set_guielements(pp);
	paras->msg_.push(L"Running script..");
	lua.start_script();
	fst->close();
	fst=NULL;
	paras->msg_.push(L"加工方案已生成.\n");
	paras->bexit = true;
}

void controller::StartLuaCompileThread(std::map<int,int>&pp)
{
	boost::shared_ptr<thread_paras> paras(new thread_paras());
	boost::thread th(boost::bind(&controller::LuaCompileFunc, this, paras, pp));

	paras->thread_first->lock();
	view->ShowProgressDlg(paras);
}

int add_interface(lua_State* l)
{
	(*fst)<<lua_tointeger(l,1)<<";"<<
		lua_tointeger(l,2)<<";"<<
		lua_tointeger(l,3)<<endl;
	return 1;
}

/**
* solution parser
* @param sln         [description]
* @param spd         [description]
* @param pointInterV [description]
*/
void controller::ProcessSolution(const Solution& solution)//const vector<slnn>& sln,int spd,int pointInterV)
{
	const vector<slnn> &sln_ = solution.sln;

	if(sln_.empty()) return ;
	//int dd=diameter;//==0?1:diameter;
	ctl->flag=ctl->f_running;
	for(size_t a=0; a!=sln_.size() && (ctl->flag==ctl->f_running) ; ++a)
	{
		view->SetPos((int)(100.f*(a+1)/sln_.size()));

		ctl->ParserSingleCommand(sln_[a]);

		if(ctl->flag==ctl->f_pause)
		{
			boost::mutex mut;
			boost::mutex::scoped_lock lock(mut);
			ctl->waker.wait(lock);
		}
		if(ctl->flag==ctl->f_stop)
			break;
	}
	//set to 100 to hide
	view->SetPos(100);
	dev->ShutterClose();
}


void controller::ParserSingleCommand(const slnn& single_cmd){
	if (!dev->stage.is_open()) return;
	auto stage_go2 = [&](){dev->stage.g2(single_cmd.x * config_.get_basexy(),single_cmd.y * config_.get_basexy());};

	auto stage_gr = [&](){
		long xxx = GetConfig().is_right_positive()? single_cmd.x : -single_cmd.x;
		long yyy = GetConfig().is_up_positive()? single_cmd.y : -single_cmd.y;
		dev->stage.gr(xxx * config_.get_basexy(), yyy * config_.get_basexy());
	};

	auto gr_with_spd = [&](){moveR(single_cmd.x ,single_cmd.y ,dev->exact_spd_axis_xy_);};

	switch(single_cmd.l)
    {
    case MoveXY_p:
        stage_go2();
        break;
    case Z_p:
        dev->stage.spz(single_cmd.y);
        dev->stage.v(single_cmd.x * config_.get_basez());
        break;
    case rZ_p:
        dev->stage.spz(single_cmd.y);
        dev->stage.zup(single_cmd.x * config_.get_basez());
        break;
    case rZ_a:
        moveZR(-single_cmd.x, single_cmd.y);
        break;
    case rMoveXY_a:
        gr_with_spd();
        break;
    case rMoveXY_p:
        stage_gr();
        break;
    case SetSpeed_a:
        dev->exact_spd_axis_xy_ = single_cmd.x;
        break;
    case SetSpeed_p:
        dev->percent_spd_axis_xy_ = single_cmd.x;
        dev->stage.sp(dev->percent_spd_axis_xy_);
        break;
    case LaserOn:
        dev->ShutterOpen();
        break;
    case LaserOff:
        dev->ShutterClose();
        break;
    case Point:
        dev->ShutterOpenWithTimeInterval(single_cmd.x);
        break;
    case Wait_ms:
        Sleep(single_cmd.x);
        break;
    }

}

void controller::SetStageCtlShutter(bool b){
	dev->SetupShutterInterfaces(b);
}

bool controller::moveZR(long zr,int speed){
	if (!dev->stage.is_open()) return false;
	long time = abs(1000*zr/speed);
	dev->stage.vz((zr>0? speed: -speed)* config_.get_basevz());
	Sleep(time);
	dev->stage.vz(0);
	return true;
}



/**
* move relatively
* @param xr    [um]
* @param yr    [um]
* @param speed [description]
*/
bool controller::moveR(long xr,long yr,int speed)
{
	if (!dev->stage.is_open()) return false;

	long cordx, cordy, cordz;
	bool res = dev->stage.getcurpos(cordx, cordy, cordz);
	if (!res) {
		lg<< "Failed: get cords."<<endl;
		return false;
	}

	int vx=0,vy=0;
	int time=0;
	long destx = cordx+ (GetConfig().is_right_positive() ? xr: -xr)*config_.get_basexy(),
		desty = cordy+ (GetConfig().is_up_positive()? yr: -yr)*config_.get_basexy();

	destx = GetConfig().is_movemode_direction_x_inverted() ? -destx : destx;
	desty = GetConfig().is_movemode_direction_y_inverted() ? -desty : desty;


	swi2fastmove(xr,yr,speed, vx, vy, time);

	lg<< "At ("<<cordx<<","<<cordy<<"),"<<"Move to ("<<destx<<","<<desty<<")"<<endl;
	dev->stage.vs(vx*config_.get_basevxy(), vy*config_.get_basevxy());
	Sleep(time);
	dev->stage.vs(0,0);
	if (GetConfig().is_hotfix_enabled()) {//correct to right cord
		lg<< "Starting hotfix."<<endl;
		dev->stage.sp(20);
		dev->stage.g2(destx, desty);
	}
	return true;
}


void controller::MoveAxisXY(long xxx, long yyy, int sp, bool laser, bool speed_mode)
{
	wxString sss(L"");
	view->Log(L"移动中...");
	boost::mutex::scoped_lock slock(mu);
	if (laser)
		ParserSingleCommand(slnn(0,0,LaserOn));
	ParserSingleCommand(slnn(sp,0,speed_mode? SetSpeed_a: SetSpeed_p));
	ParserSingleCommand(slnn(xxx,yyy,speed_mode? rMoveXY_a: rMoveXY_p));

	if (laser)
		ParserSingleCommand(slnn(0,0,LaserOff));
	//if(res)
	sss.Printf(L":)  x %ld,y %ld um.",xxx,yyy);
	//else 
	//sss.Printf(L":(  x %ld,y %ld um.",xxx,yyy);
	view->Log(sss);

}


void controller::MoveAxisZ(long zzz, int sp, bool laser, bool speed_mode){
	wxString sss(L"");
	view->Log(L"移动中...");
	boost::mutex::scoped_lock slock(mu);
	if (laser)
		ParserSingleCommand(slnn(0,0,LaserOn));

	if(speed_mode){
		zzz = ctl->GetConfig().is_movemode_direction_z_inverted()? -zzz : zzz;
	}

	ParserSingleCommand(slnn(zzz,sp,speed_mode? rZ_a: rZ_p));
	if (laser)
		ParserSingleCommand(slnn(0,0,LaserOff));

	//if(res)
	sss.Printf(L":)  Z %ld um.", zzz);
	//else 
	//sss.Printf(L":(  Z %ld um.", zzz);
	view->Log(sss);

}