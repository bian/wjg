﻿#include "const.h"
#include "devices.h"
#include "mainPanel.h"
#include "main.h"


class Application: public wxApp {
public:
  bool OnInit();
};

DECLARE_APP(Application)
IMPLEMENT_APP(Application)

bool Application::OnInit()
{
  if ( !wxApp::OnInit() ) return false;
  wxInitAllImageHandlers();
  // view
  auto mw = new MainFrame(NULL,
      wxID_ANY,
      wxT(TITLE),
      wxDefaultPosition,
      wxSize(720, 540));

  SetTopWindow(mw);
  mw->Show();
  // controller
  return true;
}