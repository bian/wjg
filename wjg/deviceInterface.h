﻿#pragma once
#include "stage.h"
#include "shutter.h"
#include "def.h"


class deviceInterface
{
public:
	deviceInterface() {};
	virtual ~deviceInterface() {};

	stageInterface stage;
	shutInterface shutter;

	virtual void SetupShutterInterfaces(bool stage_can_caontrol_shutter) = 0;
	virtual bool ShutterOpen() = 0;
	virtual bool ShutterClose() = 0;
	virtual bool ShutterOpenWithTimeInterval(int ms) = 0;
	virtual bool setup_module() = 0;

	virtual void set_percentage_speed(int speed) = 0;
	virtual void set_real_speed(int speed) = 0;
	virtual void set_point_interval(int pI) = 0;

	int percent_spd_axis_xy_, point_interval_, exact_spd_axis_xy_;
};

