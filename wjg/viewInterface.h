﻿#pragma once
#include "ctlInterface.h"

class viewInterface
{
public:
	viewInterface(){};
	virtual ~viewInterface(){};
	virtual	void Log(const wchar_t* s) = 0;
	virtual void SetPos(int percent=100) = 0;
	virtual void SetStatusbarText(const wchar_t* s, int index=0) = 0;
	virtual void SetPortConnected(bool stageport, bool connect = true) = 0;

	virtual void ShowProgressDlg(boost::shared_ptr<ctlInterface::thread_paras> paras) = 0;
};

