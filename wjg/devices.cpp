﻿
#include "stdheader.h"
#include "devices.h"
#include "def.h"


void devices::SetupShutterInterfaces(bool stage_can_caontrol_shutter){
	if (!stage_can_caontrol_shutter)
	{
		shutter_open_func_ = 	shutter.openShut;
		shutter_close_func_ = 	shutter.closeShut;
		shutter_point_func_ = shutter.openShutInterval;
	}
	else{
		shutter_open_func_ = 	stage.open_shutter;
		shutter_close_func_ = 	stage.close_shutter;
		shutter_point_func_ = stage.point;
	}
}

bool devices::setup_module(){
	return SetupInterface("stageDriver.dll", "setupStageEngine", stage) &&
		SetupInterface("shutDriver.dll", "setupShutEngine", shutter);
}

template <typename Interface>
bool devices::SetupInterface(const char* filepath, const char* func_name,
	Interface& device_interface)
{
	HINSTANCE pmod=::LoadLibraryA( filepath );
	if(!pmod)
		return false;
	else
	{
		typedef void (*SetupInterface)(Interface&);
		SetupInterface setup_func;
		setup_func= (SetupInterface)::GetProcAddress(pmod, func_name);
		setup_func(device_interface);
	}
	return true;
}

