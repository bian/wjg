﻿#pragma once
#include <vector>

enum workdef
{
    MoveXY_p = 0,
    Z_p,
    
    rZ_p,
    rZ_a,
    rMoveXY_p,
    rMoveXY_a,
    SetSpeed_p,
    SetSpeed_a,
    LaserOn,
    LaserOff,
    Point,
    Wait_ms


/*
	fastMoveTo = 0,
	slowMoveTo,
	Z,

	rZ,
	rFastMove,
	rSlowMove,

	set_speed,
	laseron,
	laseroff,
	point_with_default_interval,
	point_with_interval_in_X,

	rSlowMove_percentage,
	set_speed_percentage,
	rZ_actual_spd*/
};

//single command
struct slnn
{
	long x,y;
	int l;
	slnn()
	{
		x=0;
		y=0;
		l=0;
	}
	slnn(long x_,long y_,int l_)
	{
		x=x_;
		y=y_;
		l=l_;
	}
	void set(long x_,long y_,int l_)
	{
		x=x_;
		y=y_;
		l=l_;
	}
};

struct Solution
{
	std::vector<slnn> sln;//加工指令序列
};
