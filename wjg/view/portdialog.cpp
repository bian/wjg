﻿#include "const.h"
#include "portdialog.h"
#include "ctl.h"

BEGIN_EVENT_TABLE(panelPortSet, wxDialog)
    EVT_CHOICE(XRCID("chStage"),panelPortSet::onChoiceSel)
    EVT_CHOICE(XRCID("chShutter"),panelPortSet::onChoiceSel2)
    EVT_CHECKBOX(XRCID("ck_stage_control_shutter"),panelPortSet::onCheck)
END_EVENT_TABLE()

panelPortSet::panelPortSet(void)
{
    wxXmlResource::Get()->LoadDialog(this,(wxWindow*)0,L"dlgPortSet");
    chstage=XRCCTRL(*this, "chStage", wxChoice);
    chshutter=XRCCTRL(*this, "chShutter", wxChoice);
    cb_stage_control_shutter=XRCCTRL(*this, "ck_stage_control_shutter", wxCheckBox);
    
	chshutter->SetSelection(ctl->GetConfig().get_shutter_port_number()-1);
	chstage->SetSelection(ctl->GetConfig().get_stage_port_number()-1);
	cb_stage_control_shutter->SetValue(ctl->GetConfig().is_stage_ctl_shutter());
}


panelPortSet::~panelPortSet(void)
{
}

void panelPortSet::onChoiceSel(wxCommandEvent& e)
{
	int sel = e.GetSelection()+1;
	ctl->GetConfig().set_stage_port_number(sel);
    ctl->ReOpenStagePort(sel);
}
void panelPortSet::onChoiceSel2(wxCommandEvent& e)
{
	int sel = e.GetSelection()+1;
	ctl->GetConfig().set_shutter_port_number(sel);
    ctl->ReOpenShutterPort(sel);
}
void panelPortSet::onCheck(wxCommandEvent& e)
{
	ctl->GetConfig().set_stage_ctl_shutter(cb_stage_control_shutter->IsChecked());
}

