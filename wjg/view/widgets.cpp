﻿
#include "const.h"

#include "widgets.h"

BEGIN_EVENT_TABLE(TimedGauge, wxGauge)
	EVT_PAINT(TimedGauge::Paint)
	//EVT_SIZE(scrollCanvas::OnSize)
	//EVT_ERASE_BACKGROUND(drawBg)
END_EVENT_TABLE()

TimedGauge::TimedGauge(wxWindow*p,wxWindowID id,int range)
	:wxGauge(p,id,range)
{
	firstSet=true;
	showed_text_=L"...";
	//this->SetBackgroundStyle(wxBG_STYLE_CUSTOM);
}

void TimedGauge::Paint( wxPaintEvent &e )
{
	wxPaintDC dc(this);
	//wxBufferedPaintDC dc(this);
	dc.Clear();
	// and then carry out business as usual
	wxGauge::OnPaint(e);
	// This is the size available to us.
	int w, h ;
	this->GetSize(&w,&h);

	// Copy the default font, make it bold.
	static wxFont fn = wxFont(*wxSWISS_FONT).MakeBold();
	// Set the font for the DC ...
	dc.SetFont(fn);
	// ... and calculate how much space our value
	// will take up.
	wxSize txtsz = dc.GetTextExtent(showed_text_);

	// Calc the center of the gauge_, and from that
	// derive the origin of our value.
	int center = w / 2;
	int tx = center - (txtsz.GetWidth()/2);

	center = h / 2;
	int ty = center - (txtsz.GetHeight()/2);

	// I draw the value twice so as to give it a pseudo-shadow.
	// This is (mostly) because I'm too lazy to figure out how
	// to blit my text onto the gauge_ using one of the logical
	// functions. The pseudo-shadow gives the text contrast
	// regardless of whether the bar is under it or not.
	dc.SetTextForeground(*wxBLACK);
	dc.DrawText(showed_text_, tx, ty);

	dc.SetTextForeground(*wxWHITE);
	dc.DrawText(showed_text_, tx-1, ty-1);
}

void TimedGauge::SetPos(int p)
{
	if(p==100)
	{
		firstSet=true;
		this->Show(false);
		return;
	}
	//如果是第一次设置
	if(firstSet)
	{
		started_time_ = wxGetLocalTime();
		firstSet=false;
		showed_text_=L"wait...";
		this->Show(true);
	}
	else
	{
		//计算过去了多长时间->速率->剩余时间
		unsigned long timeCur=wxGetLocalTime();
		long timeElapsed=timeCur-started_time_;
		if(timeElapsed<0){
			this->SetValue(p);
			return;
		}
		static const int max=100;
		long remainTime=long(   (   (timeElapsed+0.)/p   )*(max-p)  );

        unsigned long hours = remainTime / 3600;
        unsigned long minutes = (remainTime % 3600) / 60;
        unsigned long seconds = remainTime % 60;
        showed_text_.Printf(wxT("%lu:%02lu:%02lu"), hours, minutes, seconds);

	}
//计算剩余时间
	this->SetValue(p);
}
