﻿///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wiz.h"

///////////////////////////////////////////////////////////////////////////

wizard::wizard( wxWindow* parent, wxWindowID id, const wxString& title, const wxBitmap& bitmap, const wxPoint& pos, long style ) 
{
	this->Create( parent, id, title, bitmap, pos, style );
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxWizardPageSimple* m_wizPage1 = new wxWizardPageSimple( this );
	m_pages.Add( m_wizPage1 );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText18 = new wxStaticText( m_wizPage1, wxID_ANY, wxT("方案输出"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer30->Add( m_staticText18, 0, wxALL, 5 );
	
	m_staticline6 = new wxStaticLine( m_wizPage1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer30->Add( m_staticline6, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer29->Add( bSizer30, 0, wxEXPAND, 5 );
	
	txoutput = new wxTextCtrl( m_wizPage1, wxID_ANY, wxT("等待生成加工方案..\n----------------------\n"), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	bSizer29->Add( txoutput, 1, wxALL|wxEXPAND, 5 );
	
	
	m_wizPage1->SetSizer( bSizer29 );
	m_wizPage1->Layout();
	bSizer29->Fit( m_wizPage1 );
	wxWizardPageSimple* m_wizPage2 = new wxWizardPageSimple( this );
	m_pages.Add( m_wizPage2 );
	
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer301;
	bSizer301 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText181 = new wxStaticText( m_wizPage2, wxID_ANY, wxT("加工区域检验"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText181->Wrap( -1 );
	bSizer301->Add( m_staticText181, 0, wxALL, 5 );
	
	m_staticline61 = new wxStaticLine( m_wizPage2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer301->Add( m_staticline61, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer32->Add( bSizer301, 0, wxEXPAND, 5 );
	
	wxGridSizer* gSizer2;
	gSizer2 = new wxGridSizer( 3, 2, 0, 0 );
	
	m_button16 = new wxButton( m_wizPage2, wxID_ANY, wxT("┌  "), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer2->Add( m_button16, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_button17 = new wxButton( m_wizPage2, wxID_ANY, wxT("┐"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer2->Add( m_button17, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_button18 = new wxButton( m_wizPage2, wxID_ANY, wxT("└"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer2->Add( m_button18, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_button19 = new wxButton( m_wizPage2, wxID_ANY, wxT("┘"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer2->Add( m_button19, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer32->Add( gSizer2, 1, wxEXPAND|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	wxBoxSizer* bSizer3011;
	bSizer3011 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText1811 = new wxStaticText( m_wizPage2, wxID_ANY, wxT("加工区域设置"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1811->Wrap( -1 );
	bSizer3011->Add( m_staticText1811, 0, wxALL, 5 );
	
	m_staticline611 = new wxStaticLine( m_wizPage2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer3011->Add( m_staticline611, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer32->Add( bSizer3011, 0, wxEXPAND, 5 );
	
	wxGridSizer* gSizer3;
	gSizer3 = new wxGridSizer( 2, 2, 0, 0 );
	
	m_button20 = new wxButton( m_wizPage2, wxID_ANY, wxT("┏"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer3->Add( m_button20, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	gSizer3->Add( 0, 0, 1, wxEXPAND, 5 );
	
	
	gSizer3->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_button21 = new wxButton( m_wizPage2, wxID_ANY, wxT("┛"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer3->Add( m_button21, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer32->Add( gSizer3, 1, wxEXPAND, 5 );
	
	
	m_wizPage2->SetSizer( bSizer32 );
	m_wizPage2->Layout();
	bSizer32->Fit( m_wizPage2 );
	
	this->Centre( wxBOTH );
	
	for ( unsigned int i = 1; i < m_pages.GetCount(); i++ )
	{
		m_pages.Item( i )->SetPrev( m_pages.Item( i - 1 ) );
		m_pages.Item( i - 1 )->SetNext( m_pages.Item( i ) );
	}
	
	// Connect Events
	m_button16->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_lefttop ), NULL, this );
	m_button17->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_righttop ), NULL, this );
	m_button18->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_leftbottom ), NULL, this );
	m_button19->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_rightbottom ), NULL, this );
	m_button20->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_set_lt ), NULL, this );
	m_button21->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_set_rb ), NULL, this );
}

wizard::~wizard()
{
	// Disconnect Events
	m_button16->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_lefttop ), NULL, this );
	m_button17->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_righttop ), NULL, this );
	m_button18->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_leftbottom ), NULL, this );
	m_button19->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_rightbottom ), NULL, this );
	m_button20->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_set_lt ), NULL, this );
	m_button21->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wizard::onbt_set_rb ), NULL, this );
	
	m_pages.Clear();
}
