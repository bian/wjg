﻿///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __WIZ_H__
#define __WIZ_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/statline.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/wizard.h>
#include <wx/dynarray.h>
WX_DEFINE_ARRAY_PTR( wxWizardPageSimple*, WizardPages );

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wizard
///////////////////////////////////////////////////////////////////////////////
class wizard : public wxWizard 
{
	private:
	
	protected:
		wxStaticText* m_staticText18;
		wxStaticLine* m_staticline6;
		wxStaticText* m_staticText181;
		wxStaticLine* m_staticline61;
		wxButton* m_button16;
		wxButton* m_button17;
		wxButton* m_button18;
		wxButton* m_button19;
		wxStaticText* m_staticText1811;
		wxStaticLine* m_staticline611;
		wxButton* m_button20;
		wxButton* m_button21;
		
		// Virtual event handlers, overide them in your derived class
		virtual void onbt_lefttop( wxCommandEvent& event ) { event.Skip(); }
		virtual void onbt_righttop( wxCommandEvent& event ) { event.Skip(); }
		virtual void onbt_leftbottom( wxCommandEvent& event ) { event.Skip(); }
		virtual void onbt_rightbottom( wxCommandEvent& event ) { event.Skip(); }
		virtual void onbt_set_lt( wxCommandEvent& event ) { event.Skip(); }
		virtual void onbt_set_rb( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxTextCtrl* txoutput;
		
		wizard( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("微加工向导"), const wxBitmap& bitmap = wxBitmap( wxT("res/Desert.png"), wxBITMAP_TYPE_ANY ), const wxPoint& pos = wxDefaultPosition, long style = wxDEFAULT_DIALOG_STYLE );
		WizardPages m_pages;
		~wizard();
	
};

#endif //__WIZ_H__
