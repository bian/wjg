﻿#include "wizd.h"


wizd::wizd( wxWindow* parent )
:
wizard( parent )
{
	wxWindow *win = wxWindow::FindWindowById(wxID_FORWARD);
	if(win) win->Disable();
	Connect(EVT_WIZ, (wxObjectEventFunction)&wizd::onWiz);
}

void wizd::onbt_lefttop( wxCommandEvent& event )
{
// TODO: Implement onbt_lefttop
}

void wizd::onbt_righttop( wxCommandEvent& event )
{
// TODO: Implement onbt_righttop
}

void wizd::onbt_leftbottom( wxCommandEvent& event )
{
// TODO: Implement onbt_leftbottom
}

void wizd::onbt_rightbottom( wxCommandEvent& event )
{
// TODO: Implement onbt_rightbottom
}

void wizd::onbt_set_lt( wxCommandEvent& event )
{
// TODO: Implement onbt_set_lt
}

void wizd::onbt_set_rb( wxCommandEvent& event )
{
// TODO: Implement onbt_set_rb
}

void wizd::onWiz( wizEvent& event )
{	
	if (event.bappend)
	{
		this->txoutput->AppendText(event.str);
	}
	else if(event.benablebt)
	{
		wxWindow *win = wxWindow::FindWindowById(wxID_FORWARD);
		if(win) win->Enable();
	}
}	

void wizd::appendtext(const wxString& t){
	wizEvent e(EVT_WIZ);
	e.str = t;
	e.bappend = true;
	this->AddPendingEvent(e);
}
void wizd::enablenextbt(){
	wizEvent e(EVT_WIZ);
	e.benablebt = true;
	this->AddPendingEvent(e);
}