﻿#pragma once

#include "viewInterface.h"


class mainPanel;
class TimedGauge;
class MainFrame : public wxFrame, public viewInterface
{
	enum UI_ID
	{
		ID_TB_START = wxID_HIGHEST+1,
		ID_TB_PAUSE,
		ID_TB_STOP,
		ID_TB_GENERATE,
		ID_TB_SHUT,
		ID_TB_CHECKBOX,
		ID_TB_TIMEOUT,
		ID_TB_PREVIEW,
		ID_GAUGE
	};
	/*enum LOG_FLAG
	{
		NORMAL=0,
		HEADER,
		ERROR,
		END,
	};*/

	public:
	MainFrame(wxWindow* parent,
			wxWindowID id,
			const wxString& title,
			const wxPoint& pos = wxDefaultPosition,
			const wxSize& size = wxDefaultSize,
			long style = wxDEFAULT_FRAME_STYLE | wxSUNKEN_BORDER);

	~MainFrame();

	class UserEvent: public wxEvent
	{
	public:
		UserEvent(wxEventType eventType)
			: wxEvent(0, eventType), percent_(100)
		{};
		// accessors
		wxString msg_;
		int statusbar_index_;//0 base
		//gauge_ evt
		int percent_;
		// implement the base class pure virtual
		virtual wxEvent *Clone() const { return new UserEvent(*this); }
	};

	void SetPos(int percent=100);
	void Log(const wchar_t* s){SetStatusbarText(s,0);};
	void SetStatusbarText(const wchar_t* s, int index=0);
	void SetPortConnected(bool stageport, bool connect);
	void ShowProgressDlg(boost::shared_ptr<ctlInterface::thread_paras> paras);

	private:
	inline void InitResources()
	{
		wxXmlResource::Get()->InitAllHandlers();
		if (!wxXmlResource::Get()->Load(L"res\\weijg.xrc") )
			wxMessageBox(_T("resource file not found."),_T(TITLE));
		image_list_=new wxImageList();
		image_list_->Create(16,16,true,0);
		image_list_->Add(wxBitmap(_T("res\\start.png"),wxBITMAP_TYPE_PNG));
		image_list_->Add(wxBitmap(_T("res\\pause.png"),wxBITMAP_TYPE_PNG));
		image_list_->Add(wxBitmap(_T("res\\stop.png"),wxBITMAP_TYPE_PNG));
	}

	void addToolBar();
	//menu
	void OnSetPortMenuEvent(wxCommandEvent& e);
	void OnSetPortMouseEvent(wxMouseEvent& e);
	void onSetXY0(wxCommandEvent& e);
	void onSetCord(wxCommandEvent& e);
	void onStopStage(wxCommandEvent& e);
	void onAbout(wxCommandEvent& e);
	void OnShowHelpDocMenuEvt(wxCommandEvent& e);
	//toolbar
	void onStart(wxCommandEvent& e);
	void onPause(wxCommandEvent& e);
	void onStop(wxCommandEvent& e);
	void onPoint(wxCommandEvent& e);
	void onUseTimer(wxCommandEvent& e);
	void onPreview(wxCommandEvent& e);

	//
	void onUpdateStatusBar(UserEvent&e);
	void onUpdateGauge(UserEvent&e);
	void onLogger(UserEvent&e);
	void OnSize(wxSizeEvent&e);

	wxTextCtrl *timer_textctl_;
	mainPanel *panel_;
	wxImageList* image_list_;
	TimedGauge *gauge_;
	wxStaticBitmap * stage_port_indicator_bitmap_, *shutter_port_indicator_bitmap_;

	DECLARE_EVENT_TABLE()
};


