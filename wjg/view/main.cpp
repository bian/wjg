﻿///////////////////////////////////////////////////////////////////////////////
// Author:      bian
// Modified by:
// Created:     2011-7-02
// Copyright:   (C) Copyright 2011-2012, All Rights Reserved.
///////////////////////////////////////////////////////////////////////////////
#include "const.h"

#include "main.h"

#include "ctl.h"
#include "portdialog.h"
#include "widgets.h"
#include "mainPanel.h"
#include "aboutdialog.h"
#include "logger.hpp"


DEFINE_EVENT_TYPE(EVT_STATUSBAR)
DEFINE_EVENT_TYPE(EVT_GAUGE)

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
	//menu
	EVT_MENU(XRCID("setport"),MainFrame::OnSetPortMenuEvent)
	EVT_MENU(XRCID("setXY0"),MainFrame::onSetXY0)
	EVT_MENU(XRCID("setCord"),MainFrame::onSetCord)
	EVT_MENU(XRCID("stopStage"),MainFrame::onStopStage)
	EVT_MENU(XRCID("menuAbout"),MainFrame::onAbout)
	EVT_MENU(XRCID("menuDoc"),MainFrame::OnShowHelpDocMenuEvt)
	//toolbar
	EVT_TOOL(ID_TB_START,MainFrame::onStart)
	EVT_TOOL(ID_TB_PAUSE,MainFrame::onPause)
	EVT_TOOL(ID_TB_STOP,MainFrame::onStop)
	EVT_TOOL(ID_TB_SHUT,MainFrame::onPoint)
	EVT_TOOL(ID_TB_CHECKBOX,MainFrame::onUseTimer)
	EVT_TOOL(ID_TB_PREVIEW,MainFrame::onPreview)

	EVT_SIZE(MainFrame::OnSize)

END_EVENT_TABLE()


MainFrame::MainFrame(wxWindow* parent,
		wxWindowID id,
		const wxString& title,
		const wxPoint& pos,
		const wxSize& size,
		long style)
: wxFrame(parent, id, title, pos, size, style)
{
	ctl = new controller(this);

	gauge_=(TimedGauge*)0;
	InitResources();
	lg<<"connect ui event"<<endl;
	Connect(EVT_STATUSBAR, (wxObjectEventFunction)&MainFrame::onUpdateStatusBar);
	Connect(EVT_GAUGE, (wxObjectEventFunction)&MainFrame::onUpdateGauge);

	SetIcon(wxICON(mainicon));
	SetMenuBar(wxXmlResource::Get()->LoadMenuBar(wxT("MainMenu")));
	addToolBar();
	SetMinSize(wxSize(400,200));

	wxBoxSizer *mainsz=new wxBoxSizer(wxVERTICAL);
	this->SetSizer(mainsz);
	panel_=new mainPanel(this);
	mainsz->Add(panel_,1,wxEXPAND);

	wxStatusBar* stb=CreateStatusBar(6);
	int wid[]= {-1,240,170,20,20,24};
	stb->SetStatusWidths(6,wid);

	gauge_=new TimedGauge(stb,ID_GAUGE,100);
	gauge_->Show(false);
	stage_port_indicator_bitmap_ = new wxStaticBitmap(stb, -1, wxNullBitmap);
	stage_port_indicator_bitmap_->Connect(wxEVT_LEFT_UP , (wxObjectEventFunction)&MainFrame::OnSetPortMouseEvent);
	shutter_port_indicator_bitmap_ = new wxStaticBitmap(stb, -1, wxNullBitmap);
	shutter_port_indicator_bitmap_->Connect(wxEVT_LEFT_UP , (wxObjectEventFunction)&MainFrame::OnSetPortMouseEvent);

	SetStatusText(L"Initialized.");
	Centre();
	this->SendSizeEvent();

	ctl->ReOpenStagePort();
	ctl->ReOpenShutterPort();

	lg<<"initialized."<<endl;
}

MainFrame::~MainFrame()
{
}

void MainFrame::addToolBar()
{
	wxToolBar*toolbar=new wxToolBar(this,-1,wxDefaultPosition, wxDefaultSize,wxTB_FLAT|wxTB_TEXT );
	toolbar->SetToolBitmapSize(wxSize(16,16));
	wxBitmap bm;
	bm = wxBitmap(L"res\\shutter.png", wxBITMAP_TYPE_PNG);
	toolbar->AddTool( ID_TB_SHUT, wxT("快门"), bm.ConvertToImage().Scale(16,16), wxNullBitmap, wxITEM_CHECK, wxEmptyString, wxEmptyString, NULL );
	bm = wxBitmap(L"res\\timer.png", wxBITMAP_TYPE_PNG);
	toolbar->AddTool( ID_TB_CHECKBOX, wxT("定时/ms"), bm.ConvertToImage().Scale(16,16), wxNullBitmap, wxITEM_CHECK, wxEmptyString, wxEmptyString, NULL );
	timer_textctl_ = new wxTextCtrl( toolbar, ID_TB_TIMEOUT, wxT("200"));
	toolbar->AddControl( timer_textctl_ );
	timer_textctl_->Enable(false);
	toolbar->AddSeparator();

	toolbar->AddTool(ID_TB_START, wxT("加工"), image_list_->GetBitmap(0).ConvertToImage().Scale(16,16));
	toolbar->AddTool(ID_TB_PAUSE, wxT("暂停"), image_list_->GetBitmap(1).ConvertToImage().Scale(16,16));
	toolbar->AddTool(ID_TB_STOP, wxT("停止"),image_list_->GetBitmap(2).ConvertToImage().Scale(16,16));
	toolbar->AddSeparator();
	bm = wxBitmap(L"res\\preview.png", wxBITMAP_TYPE_PNG);
	toolbar->AddTool( ID_TB_PREVIEW, wxT("加工预览"), bm.ConvertToImage().Scale(16,16));

	toolbar->Realize();
	this->SetToolBar(toolbar);
}

void MainFrame::SetPortConnected(bool stageport, bool connect){
	if (stageport)
	{
		stage_port_indicator_bitmap_->SetBitmap(wxBitmap(connect?L"res\\connect.png" : L"res\\disconnect.png",wxBITMAP_TYPE_PNG));
		stage_port_indicator_bitmap_->SetToolTip(connect? L"平台端口已经连通" : L"平台端口未连接");
		// stage_port_indicator_bitmap_->Refresh();
	}
	else{
		shutter_port_indicator_bitmap_->SetBitmap(wxBitmap(connect?L"res\\connect.png" : L"res\\disconnect.png",wxBITMAP_TYPE_PNG));
		shutter_port_indicator_bitmap_->SetToolTip(connect? L"Shutter 端口已经连通" : L"Shutter 端口未连接");
		// shutter_port_indicator_bitmap_->Refresh();
	}
	this->SendSizeEvent();
}

void MainFrame::OnSetPortMenuEvent(wxCommandEvent& WXUNUSED(e))
{
	panelPortSet ps;
	ps.ShowModal();
}
void MainFrame::OnSetPortMouseEvent(wxMouseEvent& e){
	panelPortSet ps;
	ps.ShowModal();
}
void MainFrame::OnShowHelpDocMenuEvt(wxCommandEvent& WXUNUSED(e))
{
	ShellExecuteA(NULL,"open","doc\\manual.html",NULL, NULL, SW_SHOWNORMAL);

}
void MainFrame::onAbout(wxCommandEvent& WXUNUSED(e))
{
	aboutdialog dlg(this, -1, L"About");
	dlg.ShowModal();
}
void MainFrame::onSetXY0(wxCommandEvent& e)
{
	ctl->SetXYCord0();
}
void MainFrame::onSetCord(wxCommandEvent& e)
{
	wxTextEntryDialog dlg(this,L"输入(x,y,z)坐标,单位um",L"坐标间用“;”隔开，留空项表示不改变",L";;");
	if( dlg.ShowModal()==wxID_OK )
	{
		wxString ret=dlg.GetValue();
		wxArrayString sss=wxStringTokenize(ret,wxT(";"));
		long ttt;
		if(!sss[0].empty()) {sss[0].ToLong(&ttt); ctl->SetCord(0,ttt); }
		if(!sss[1].empty()) {sss[1].ToLong(&ttt); ctl->SetCord(1,ttt); }
		if(!sss[2].empty()) {sss[2].ToLong(&ttt); ctl->SetCord(2,ttt); }
	}
}

void MainFrame::onStopStage(wxCommandEvent& e)
{
	ctl->StopStageImmediately();
}

//toolbar
void MainFrame::onStart(wxCommandEvent& WXUNUSED(e))
{
	ctl->StartMachining();
}
void MainFrame::onPause(wxCommandEvent& WXUNUSED(e))
{
	if (ctl->flag==ctl->f_pause)
	{
		ctl->flag=ctl->f_running;
		ctl->waker.notify_all();
	}
	else
		ctl->flag=ctl->f_pause;

}
void MainFrame::onStop(wxCommandEvent& WXUNUSED(e))
{
	if (ctl->flag==ctl->f_pause)
	{
		ctl->flag=ctl->f_stop;
		ctl->waker.notify_all();
	}
	ctl->flag=ctl->f_stop;
}

void MainFrame::onPoint(wxCommandEvent& WXUNUSED(e))
{
	//判断是否计时
	bool toggle=this->GetToolBar()->GetToolState(ID_TB_CHECKBOX);
	if(toggle)
	{
		ctl->ShutterOpenWithTimeInterval(wxAtoi(timer_textctl_->GetValue()));
		this->GetToolBar()->ToggleTool(ID_TB_SHUT,false);
	}
	else
	{
		toggle=this->GetToolBar()->GetToolState(ID_TB_SHUT);
		if(toggle)
			ctl->ShutterOpen();
		else
			ctl->ShutterClose();
	}
}
void MainFrame::onPreview(wxCommandEvent& WXUNUSED(e)){
	ShellExecuteA(NULL,"open","sln_gl_frontend.exe",NULL, NULL, SW_SHOWNORMAL);
}
void MainFrame::onUseTimer(wxCommandEvent& WXUNUSED(e))
{
	bool toggle=this->GetToolBar()->GetToolState(ID_TB_CHECKBOX);
	timer_textctl_->Enable(toggle);


}

//自定义消息
void  MainFrame::onUpdateStatusBar(UserEvent&e)
{
	this->GetStatusBar()->SetStatusText(e.msg_,e.statusbar_index_);
}

void  MainFrame::onUpdateGauge(UserEvent&e)
{
	gauge_->SetPos(e.percent_);
}


void MainFrame::OnSize(wxSizeEvent&WXUNUSED(e))
{
	if(this->GetStatusBar() && gauge_ )
	{
		wxRect rct;
		this->GetStatusBar()->GetFieldRect(2,rct);
		gauge_->SetSize(rct);
		this->GetStatusBar()->GetFieldRect(3,rct);
		stage_port_indicator_bitmap_->SetSize(rct);
		this->GetStatusBar()->GetFieldRect(4,rct);
		shutter_port_indicator_bitmap_->SetSize(rct);
	}
	this->Layout();
}

void MainFrame::SetStatusbarText(const wchar_t* s,int index)
{
	UserEvent e(EVT_STATUSBAR);
	e.msg_=wxString(s);
	e.statusbar_index_=index;
	this->AddPendingEvent(e);
}
void MainFrame::SetPos(int percent)
{
	UserEvent e(EVT_GAUGE);
	e.percent_=percent;
	//this->AddPendingEvent(e);
	this->ProcessEvent(e);
}


void MainFrame::ShowProgressDlg(boost::shared_ptr<ctlInterface::thread_paras> paras)
{
	wxProgressDialog dialog("Generating Solution...", wxString(' ', 100),
		100,    // range
		NULL,   // parent
		wxPD_APP_MODAL |
		//wxPD_AUTO_HIDE | // -- try this as well
		wxPD_ELAPSED_TIME |
		wxPD_SMOOTH // - makes indeterminate mode bar on WinXP very small
		);
	wxString last_msg(L"");
	paras->wait_ui->unlock();
	while(!paras->bexit || last_msg==L"" || !paras->msg_.empty())
	{
		if(!paras->msg_.empty()){
			last_msg = paras->msg_.front();
			paras->msg_.pop();
		}
		dialog.Pulse(last_msg);
		Sleep(200);
	}
	Sleep(800);
}