﻿#pragma once

#include <map>

enum
{
    ID_PG_BMP=3001,
    ID_PG_IMPORTED,
    ID_PG_LUASC
};


class mainPanel :
    public wxPanel
{
public:
    mainPanel(wxWindow* parent);
    ~mainPanel(void);

private:
    wxTextCtrl *shutter_interval_for_importedfiles_;
    wxTextCtrl *percentage_speed_for_importedfiles_;

    void OnGenerateSolutionForImportedfiles(wxCommandEvent& e);
    void OnGenerateSolutionForLuaScripts(wxCommandEvent& e);
    void OnRefreshPlugins(wxCommandEvent& e);
    void OnMoveAxisXY(wxCommandEvent& e);
    void OnMoveAxisZ(wxCommandEvent& e);

    void OnMoveModeChangedAxisXY(wxCommandEvent& e);
    void OnMoveModeChangedAxisZ(wxCommandEvent& e);

    //void onRunScript(wxCommandEvent& e);
    
    wxTextCtrl *axis_paras_textctl_[4];
    void LinkUpUI();
    void ResetPlugins();

    DECLARE_EVENT_TABLE()
};

inline void translateCordFromLabel(const wxString& name,long&xxx,long&yyy)
{
    if(name == L"btzs") {xxx=-xxx; yyy=-yyy; }
    else if(name == L"bts") {xxx=0; yyy=-yyy; }
    else if(name == L"btys") {yyy=-yyy; } 
    else if(name == L"btz") {xxx=-xxx; yyy=0; }
    else if(name == L"bty") {yyy=0; } 
    else if(name == L"btzx") {xxx=-xxx; } 
    else if(name == L"btx") {xxx=0; } 
    else if(name == L"btyx")
    {} 
};
