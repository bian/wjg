﻿//const.h
#pragma once

class panelPortSet :
	public wxDialog
{
public:
	panelPortSet(void);
	~panelPortSet(void);

private:
	wxChoice *chstage, *chshutter;
	wxCheckBox* cb_stage_control_shutter;
	void onChoiceSel(wxCommandEvent& e);
	void onChoiceSel2(wxCommandEvent& e);
	void onCheck(wxCommandEvent& e);
	DECLARE_EVENT_TABLE()
};

