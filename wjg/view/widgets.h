﻿#pragma once

class TimedGauge :public wxGauge
{
public :
	TimedGauge(wxWindow*p,wxWindowID id,int range);
	~TimedGauge(){}
	void SetPos(int p);
	bool firstSet;
private:
	unsigned long started_time_;
	wxString showed_text_;
	void Paint( wxPaintEvent &e );
	DECLARE_EVENT_TABLE()
};
