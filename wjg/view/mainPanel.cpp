﻿
#include "const.h"
#include "ctl.h"
#include "viewInterface.h"
#include "widgets.h"
#include "mainPanel.h"
#include "locale.h"
#include "ago.h"
using namespace std;

#define bind_button_id(id) EVT_BUTTON(XRCID(id),mainPanel:: OnMoveAxisXY)
#define bind_buttonz_id(id) EVT_BUTTON(XRCID(id),mainPanel:: OnMoveAxisZ)

BEGIN_EVENT_TABLE(mainPanel, wxPanel)
	bind_button_id("btzs")
	bind_button_id("bts")
	bind_button_id("btys")
	bind_button_id("btz")
	bind_button_id("bty")
	bind_button_id("btzx")
	bind_button_id("btx")
	bind_button_id("btyx")

	bind_buttonz_id("btZs")
	bind_buttonz_id("btZx")
	//
	EVT_BUTTON(XRCID("btGen2d3d"),mainPanel:: OnGenerateSolutionForImportedfiles)
	EVT_BUTTON(XRCID("btRefreshPlugins"),mainPanel:: OnRefreshPlugins)
	EVT_BUTTON(XRCID("btGenStruct"),mainPanel:: OnGenerateSolutionForLuaScripts)

	EVT_RADIOBOX(XRCID("m_radioBox11"),mainPanel::OnMoveModeChangedAxisXY)
	EVT_RADIOBOX(XRCID("m_radioBox1"),mainPanel::OnMoveModeChangedAxisZ)

	//EVT_BUTTON(XRCID("btGenStruct1"),mainPanel:: onRunScript)
END_EVENT_TABLE()



mainPanel::mainPanel(wxWindow* parent)
{
	wxXmlResource::Get()->LoadPanel(this,parent,L"mainPanel");
	LinkUpUI();

	ResetPlugins();
	
	int m_notebook2Index = 0;
	wxImageList* m_notebook2Images = new wxImageList( 16,16);
	wxNotebook * m_notebook2 = XRCCTRL(*this,"m_notebook2",wxNotebook);
	m_notebook2->SetPadding(wxSize(6,6));
	m_notebook2->AssignImageList( m_notebook2Images );
	wxBitmap m_notebook2Bitmap;
	wxImage m_notebook2Image;
	auto add_bitmap = [&m_notebook2Bitmap, &m_notebook2Image, m_notebook2Images, m_notebook2, &m_notebook2Index](const wxString& path){
		m_notebook2Bitmap = wxBitmap(path , wxBITMAP_TYPE_ANY );
		if ( m_notebook2Bitmap.Ok() )
		{
			m_notebook2Image = m_notebook2Bitmap.ConvertToImage();
			m_notebook2Images->Add( m_notebook2Image.Scale(16,16));
			m_notebook2->SetPageImage( m_notebook2Index, m_notebook2Index );
			m_notebook2Index++;
		}
	};
	add_bitmap(L"res/tab1.png");
	add_bitmap(L"res/tab2.png");
	add_bitmap(L"res/tab3.png");

	setlocale( LC_ALL, "chs");


}

mainPanel::~mainPanel(void)
{

}

void mainPanel::LinkUpUI()
{
	//tab1
	this->axis_paras_textctl_[0]=XRCCTRL(*this,"textpx",wxTextCtrl);
	this->axis_paras_textctl_[1]=XRCCTRL(*this,"textpy",wxTextCtrl);
	this->axis_paras_textctl_[2]=XRCCTRL(*this,"m_textCtrlZ",wxTextCtrl);
	this->axis_paras_textctl_[3]=XRCCTRL(*this,"textps",wxTextCtrl);

	shutter_interval_for_importedfiles_=XRCCTRL(*this,"txtPIL",wxTextCtrl);
	percentage_speed_for_importedfiles_=XRCCTRL(*this,"txtSpeed1",wxTextCtrl);
}

void mainPanel::ResetPlugins()
{
	wxListbook*list=XRCCTRL(*this, "listBookStruct", wxListbook);
	list->DeleteAllPages();
	ctl->luaPlugins.clear();

	wxArrayString files;
	wxDir dir(wxGetCwd());
	if(wxDir::Exists(L"Plugins"))
		dir.GetAllFiles(L"Plugins",&files,wxT("*.lua"),wxDIR_FILES | wxDIR_HIDDEN);

	for(size_t i=0; i<files.GetCount(); i++)
	{
		if(!ctl->lua.run_lua_scripts(files.Item(i).mb_str())) {
			msg(L"An error occured in "+files.Item(i));
			continue;
		}
		char name[255];
		ctl->lua.get_name(name);
		ctlInterface::luaPlugin newlp( files.Item(i).mb_str());
		ctl->lua.get_guielements(newlp.key, newlp.value);
		ctl->luaPlugins.insert(pair<int, ctlInterface::luaPlugin>(i, newlp) );
		wxPropertyGrid*  pg=new wxPropertyGrid(list, ID_PG_LUASC, wxDefaultPosition, \
				wxDefaultSize, wxPG_BOLD_MODIFIED|wxPG_DEFAULT_STYLE|wxPG_SPLITTER_AUTO_CENTER);
		list->AddPage(pg,wxString(name, wxConvUTF8),false);

		auto pi=newlp.key.begin();
		auto pvi=newlp.value.begin();
		for(; pi!=newlp.key.end(); pi++,pvi++)
		{
			pg->Append(new wxIntProperty(wxString(pi->second.c_str(), wxConvUTF8), wxPG_LABEL, pvi->second) );
		}
	}
}


void mainPanel::OnGenerateSolutionForImportedfiles(wxCommandEvent& WXUNUSED(e)) {
	wxString file_path=XRCCTRL(*this,"pickBmp",wxFilePickerCtrl)->GetPath();
	int dia= wxAtoi(XRCCTRL(*this,"txtFocusDia",wxTextCtrl)->GetValue());
	int speed = wxAtoi(percentage_speed_for_importedfiles_->GetValue());
	int interval = wxAtoi(shutter_interval_for_importedfiles_->GetValue());
	
	ctl->StartGenerateSolutionForImportedFilesThread(file_path.c_str(), dia, speed, interval);
}
void mainPanel::OnGenerateSolutionForLuaScripts(wxCommandEvent& WXUNUSED(e)) {
	wxListbook*lb=XRCCTRL(*this,"listBookStruct",wxListbook);
	size_t index=lb->GetSelection();
	wxPropertyGrid*pg=(wxPropertyGrid*)(lb->GetPage(index));
	wxPropertyGridIterator pgit=pg->GetIterator();

	auto pp= ctl->luaPlugins.find(index);

	if(!ctl->lua.run_lua_scripts(pp->second.filepath.c_str())) {
		msg(L"Script has errors. Please check it!");
		return;
	}
	
	auto pi=pp->second.value.begin();
	int ki=0;
	for(; pi!=pp->second.value.end() && !pgit.AtEnd(); pi++)
	{
		pi->second=int(pgit.GetProperty()->GetValue().GetLong());
		ki++;
		pgit++;
	}
	ctl->StartLuaCompileThread(pp->second.value);

}

void mainPanel::OnRefreshPlugins(wxCommandEvent& WXUNUSED(e)) {
	ResetPlugins();
}


void mainPanel::OnMoveAxisXY(wxCommandEvent& e) {
	bool laser=XRCCTRL(*this,"useLaser",wxCheckBox)->IsChecked();
	long xxx, yyy, sp;
	axis_paras_textctl_[0]->GetValue().ToLong(&xxx);
	axis_paras_textctl_[1]->GetValue().ToLong(&yyy);
	axis_paras_textctl_[3]->GetValue().ToLong(&sp);
	translateCordFromLabel(((wxButton*)e.GetEventObject())->GetName(),xxx,yyy);

	wxRadioBox* rb=XRCCTRL(*this,"m_radioBox11",wxRadioBox);
	bool speed_mode = (rb->GetSelection()==1);
	if(!speed_mode)
		sp = XRCCTRL(*this,"spin_spz1",wxSpinCtrl)->GetValue();
	boost::thread move_stage_thread(
		boost::bind(&ctlInterface::MoveAxisXY,
		ctl, xxx, yyy, sp, laser, speed_mode));
}

void mainPanel::OnMoveAxisZ(wxCommandEvent& e)
{
	bool laser=XRCCTRL(*this,"useLaser1",wxCheckBox)->IsChecked();
	int sp;
	long zzz;
	axis_paras_textctl_[2]->GetValue().ToLong(&zzz);
	if(((wxButton*)e.GetEventObject())->GetName()!=wxT("btZs")) 
		zzz=-zzz;
	zzz = ctl->GetConfig().is_zup_positive()?zzz:-zzz;

	wxRadioBox* rb=XRCCTRL(*this,"m_radioBox1",wxRadioBox);
	bool speed_mode = (rb->GetSelection()==1);
	
	if(speed_mode)
		zzz = ctl->GetConfig().is_z_exchanged()?-zzz:zzz;
	if(speed_mode)
		sp = wxAtoi(XRCCTRL(*this,"m_textCtrl9",wxTextCtrl)->GetValue());
	else{
		sp = XRCCTRL(*this,"spin_spz",wxSpinCtrl)->GetValue();
	}

	boost::thread move_stage_thread(
		boost::bind(&ctlInterface::MoveAxisZ, ctl,
		zzz, sp, laser, speed_mode));
}

void mainPanel::OnMoveModeChangedAxisXY(wxCommandEvent& e){
	wxRadioBox* rb=XRCCTRL(*this,"m_radioBox11",wxRadioBox);
	int sel = rb->GetSelection();
	XRCCTRL(*this,"textps",wxTextCtrl)->Enable(sel == 1);
	XRCCTRL(*this,"m_staticText91",wxStaticText)->Enable(sel == 1);
	XRCCTRL(*this,"m_staticText29",wxStaticText)->Enable(sel == 1);

	XRCCTRL(*this,"spin_spz1",wxSpinCtrl)->Enable(sel == 0);
	XRCCTRL(*this,"m_staticText81",wxStaticText)->Enable(sel == 0);
	XRCCTRL(*this,"st_speed2",wxStaticText)->Enable(sel == 0);
}

void mainPanel::OnMoveModeChangedAxisZ(wxCommandEvent& e){
	wxRadioBox* rb=XRCCTRL(*this,"m_radioBox1",wxRadioBox);
	int sel = rb->GetSelection();
	XRCCTRL(*this,"m_textCtrl9",wxTextCtrl)->Enable(sel == 1);
	XRCCTRL(*this,"sdssd1",wxStaticText)->Enable(sel == 1);
	XRCCTRL(*this,"st_speed1",wxStaticText)->Enable(sel == 1);

	XRCCTRL(*this,"spin_spz",wxSpinCtrl)->Enable(sel == 0);
	XRCCTRL(*this,"sdssd",wxStaticText)->Enable(sel == 0);
	XRCCTRL(*this,"st_speed",wxStaticText)->Enable(sel == 0);
}
/*
void mainPanel::onRunScript(wxCommandEvent& e){
	

}
*/