﻿#pragma once

#include "stdheader.h"

#include "wx/wx.h"
#include "wx/wxprec.h"
#include "wx/event.h"
#include "wx/app.h"
#include "wx/dir.h"
#include "wx/file.h"
#include "wx/listbook.h"
#include "wx/listctrl.h"
#include "wx/filepicker.h"
#include "wx/image.h"
#include "wx/rawbmp.h"
#include "wx/dialog.h"
#include "wx/splitter.h"
#include "wx/xrc/xmlres.h"
#include "wx/tokenzr.h"
#include "wx/dcbuffer.h"
#include "wx/spinctrl.h"
#include "wx/propgrid/propgrid.h"
#include "wx/progdlg.h"

#define TITLE "微加工"
#define msg wxMessageBox
#define msgd(d) do{char s[255]; sprintf(s,"%d",d);wxMessageBox(s);}while(0)


class logger;
extern logger lg;