plugin_name,version = "光栅 (百分比速度)", "2012.12.21"

guiElements={
[1]="光栅线宽/um",
[2]="光栅间距/um",
[3]="激光光斑直径(须为线宽和间距的公约数)/um",
[4]="总区域长/um",
[5]="总区域高/um",
[6]="移动速度(%)",
}

guiValue={
[1]=20,
[2]=30,
[3]=10,
[4]=100,
[5]=200,
[6]=4,
}

-- import defines
require "libs.commondef"

function add_elements()
	local w_=guiValue[1]
	local h_=guiValue[2]
	local d=guiValue[3]
	local w=guiValue[4]
	local h=guiValue[5]
	local spd=guiValue[6]
	local T=h/(w_+h_)
	local t=w_/d
	local l2r=true


	for i=0,T-1 do
		if t==1 then
			add(0,0,LaserOn)
			add(spd, 0, SetSpeed_p)
			if l2r then
				add(w,0,rMoveXY_p)
			else
				add(-w,0,rMoveXY_p)
			end
			add(0,0,LaserOff)
			l2r=not l2r
		else
			add(0,0,LaserOn)
			add(spd, 0, SetSpeed_p)
			if l2r then
				add(w,0,rMoveXY_p)
			else
				add(-w,0,rMoveXY_p)
			end

			for j=0,t-1 do
				l2r=not l2r
				if j~=t-1 then
					add(0,d,rMoveXY_p)
					if l2r then
						add(w,0,rMoveXY_p)
					else
						add(-w,0,rMoveXY_p)
					end
					if j+2 ==t then
						add(0,0,LaserOff)
					end
				end
			end
		end

		if(i~=T-1) then
			add(50, 0, SetSpeed_p)
			add(0,h_+d, rMoveXY_p)
		end
	end
end


