plugin_name, version = "圆", "2012.12.21"
-- import defines
require "libs.commondef"

guiElements={
	[1]="圆的半径/um",
	[2]="圆的边数(有多圆)",
	[3]="移动速度(%)",
}


guiValue={
	[1]=50,
	[2]=60,
	[3]=4,
}

function add_elements()
	local radius=guiValue[1]
	local edgeNumber=guiValue[2]
	local spd=guiValue[3]

	--calculate the every points
	local lastx=2*radius
	local lasty=radius
	local firstPoint=true

	add(50, 0, SetSpeed_p)
	add(2*radius,radius, rMoveXY_p)
	add(spd, 0, SetSpeed_p)
    add(0,0, LaserOn)
	for i=1,edgeNumber do
		local x=math.cos(math.pi*2*i / edgeNumber)*radius + radius
		local y=math.sin(math.pi*2*i / edgeNumber)*radius + radius
		
		--delete the identical points
		if lastx~=x or lasty~=y then
			add(x-lastx,y-lasty, rMoveXY_p)
		end
		--remember the last cordinates
		lastx=x
		lasty=y
	end

	add(0,0,LaserOff)

end



