﻿plugin_name,version = "螺旋", "2013.3.21"

guiElements={
[1]="投影于xy平面的圆的半径(um)",
[2]="Z轴上升步进(um)",
[3]="投影于xy平面的圆的边数",
[4]="Z轴高度(um)",
[5]="加工速度(1-100%)"
}

guiValue={
[1]=100,
[2]=7,
[3]=60,
[4]=250,
[5]=5
}

-- import defines
require "libs.commondef"

function add_elements()
    local radius=guiValue[1]
    local z_stepsize=guiValue[2]
    local edgeNumber=guiValue[3]
    local z_height=guiValue[4]
    local spd=guiValue[5]
    --calculate the every points
    local lastx=radius
    local lasty=0
    local steps = z_height/z_stepsize
    add(spd,0, SetSpeed_p)
    add(0,0,LaserOn)
    i = 1
    for index=1,steps do
        local x=math.cos(math.pi*2*i / edgeNumber)*radius
        local y=math.sin(math.pi*2*i / edgeNumber)*radius
        i = i+1
        --delete the identical points
        if lastx~=x or lasty~=y then
            add(x-lastx,y-lasty, rMoveXY_p)
            add(-z_stepsize,4, rZ_p)
        end
        --remember the last cordinates
        lastx=x
        lasty=y
    end
    add(0,0,LaserOff)
end


