﻿plugin_name,version = "6芯PCF", "2013.3.20"

guiElements={
[1]="单芯直径(um)",
[2]="z轴扫描速度(%)",
[3]="z轴深度(um)",
[4]="层数"
}

guiValue={
[1]=20,
[2]=20,
[3]=5000,
[4]=1
}

-- import defines
require "libs.commondef"

function paintzaxis(depth, spd)
	add(depth, 20, rZ_p)
	add(0,0,LaserOn)
	add(-depth, spd, rZ_p)
	add(0,0,LaserOff)
end

function add_elements()
	local d=guiValue[1]
	local spd=guiValue[2]
	local depth=guiValue[3]
	local num=guiValue[4]

    add(50,0, SetSpeed_p)


	for ii=1,num do
		i=num-ii+1
		paintzaxis(depth, spd)
		for j=1,i do
			add(d,0,rMoveXY_p)
			paintzaxis(depth, spd)
		end
		for j=1,i do
			add(d*0.5,d*0.866,rMoveXY_p)
			paintzaxis(depth, spd)
		end
		for j=1,i do
			add(-d*0.5,d*0.866,rMoveXY_p)
			paintzaxis(depth, spd)
		end
		for j=1,i do
			add(-d,0,rMoveXY_p)
			paintzaxis(depth, spd)
		end
		for j=1,i do
			add(-d*0.5,-d*0.866,rMoveXY_p)
			paintzaxis(depth, spd)
		end
		for j=1,i do
			add(d*0.5,-d*0.866,rMoveXY_p)
			if j~=i then
				paintzaxis(depth, spd)
			end
		end
		if i~=1 then
			add(d*0.5,d*0.866,rMoveXY_p)
		end
	end
end


