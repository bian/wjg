plugin_name, version = "点阵(高级) v1.0", "2012.12.21"

guiElements={
[1]="行数",
[2]="列数",
[3]="行间距/um",
[4]="列间距/um",
[5]="行间z轴增量/um",
[6]="每行最左(起始)打点时间/ms",
[7]="从左至右打点时间增量/ms",
[8]="从左至右打点时间递增间隔/点数"
}

guiValue={
[1]=12,
[2]=3,
[3]=30,
[4]=40,
[5]=0,
[6]=200,
[7]=0,
[8]=1
}

-- import defines
require "libs.commondef"

function add_elements()
	local ll=guiValue[1]--"列数"]
	local hh=guiValue[2]--"行数"]
	local lj=guiValue[3]--"列间距/um"]
	local hj=guiValue[4]--"行间距/um"]
	local dt=guiValue[7]--"列打点时间增量/ms"]
	local dz=guiValue[5]--"行z轴增量/um"]
	local init=guiValue[6]--"行起始打点时间/ms"]
	local T=guiValue[8]--"行打点时间递增间隔/点数"]

    add(50,0, SetSpeed_p)

	for i=0,hh-1 do
		--print(i,"\n")
		if i~=0 then
			add(dz,20,rZ_p)
		end
		add(init,0,Point)
		for j=0,ll-2 do
			add(lj,0,rMoveXY_p)
			add(init+dt*((j+1)/T),0,Point)
		end
		if i~=hh-1 then
			add(-(lj*ll-lj),hj,rMoveXY_p)
		end
	end

end
