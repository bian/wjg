﻿plugin_name,version = "光栅（速度精确）", "2012.12.24"

guiElements={
[1]="光栅数目",
[2]="长度(um)",
[3]="间距(um)",
[4]="speed(um/s)"
}

guiValue={
[1]=5,
[2]=200,
[3]=20,
[4]=1
}

-- import defines
require "libs.commondef"

function add_elements()
	local num=guiValue[1]
	local len=guiValue[2]
	local vgap=guiValue[3]
	local spd=guiValue[4]
	local right = true
	local mm = 0
	add(spd, 0, SetSpeed_a)
	for i = 1, num do
		if right then
			mm = len
		else
			mm = -len
		end
		add(0,0,LaserOn)
		add(mm, 0, rMoveXY_a)
		add(0,0,LaserOff)

		if i ~= num then
			add(50, 0, SetSpeed_p)
			add(0, vgap, rMoveXY_p)
		end
		right = not right
	end

end


