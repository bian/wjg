-- import defines
require "libs.commondef"

plugin_name,version = "螺旋中有螺旋", "2013.3.22"

guiElements={
[1]="小螺旋投影于xy平面的圆的半径(um)",
[2]="Z轴上升步进(um)",
[3]="小螺旋投影于xy平面的圆的边数",
[4]="Z轴高度(um)",
[5]="大螺旋的半径(um)",
[6]="大螺旋投影于xy平面的圆的边数",
[7]="扫描速度(1-100%)"
}

guiValue={
[1]=20,
[2]=7,
[3]=30,
[4]=3000,
[5]=300,
[6]=800,
[7]=5
}


function add_elements()
    local radius=guiValue[1]
    local z_stepsize=guiValue[2]
    local edgeNumber=guiValue[3]
    local z_height=guiValue[4]
    local R=guiValue[5]
    local EdgeNumber=guiValue[6]
    local spd=guiValue[7]
    --calculate the every points
    local lastx = radius + R
    local lasty = 0
    local steps = z_height/z_stepsize
    add(spd,0, SetSpeed_p)
    add(0,0, LaserOn)
    i = 1
    for index=1,steps do
        local x=math.cos(math.pi*2*i / edgeNumber)*radius + math.cos(math.pi*2*i / EdgeNumber)*R
        local y=math.sin(math.pi*2*i / edgeNumber)*radius + math.sin(math.pi*2*i / EdgeNumber)*R
        i = i+1
        --delete the identical points
        if lastx~=x or lasty~=y then
            add(x-lastx,y-lasty, rMoveXY_p)
            add(-z_stepsize,4, rZ_p)
        end
        --remember the last cordinates
        lastx=x
        lasty=y
    end
    add(0,0, LaserOff)
end


