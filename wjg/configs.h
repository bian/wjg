#pragma once

#include "cfg.h"



#define APP "wjg"
#define CONF ".\\wjg.ini" 
#define SHUTTER_PORT "shutter_port"
#define STAGE_PORT "stage_port"
#define STAGECONTROLSHUTTER "stage_can_control_shutter"
#define BASE "converter"
#define BASEXY "converter_xy_data2um"
#define BASEZ "converter_z_data2um"
#define BASEVXY "converter_vxy_data2um"
#define BASEVZ "converter_vz_data2um"

#define MOVEMODE_DIRECTION_X_INVERTED "movemode_direction_x_inverted"
#define MOVEMODE_DIRECTION_Y_INVERTED "movemode_direction_y_inverted"
#define MOVEMODE_DIRECTION_Z_INVERTED "movemode_direction_z_inverted"

#define RIGHT_IS_POSITIVE "right_is_positive"
#define UP_IS_POSITIVE "up_is_positive"
#define ZUP_IS_POSITIVE "zup_is_positive "



#define UI_DIRECTIONS "ui_directions"
#define EXCHANGE_Z "exchange_z"



class config
{
public:
	config()
		:cfg_(new cfg(CONF))
	{
		cfg_->set_region_name(APP);
		Init();
	}
	~config(){

	}

	void Init(){

		basexy = cfg_->get_int(BASEXY, 100);
		basez=cfg_->get_int(BASEZ, 500);
		basevxy=cfg_->get_int(BASEVXY, 1);
		basevz=cfg_->get_int(BASEVZ, 30);

		right_is_positive = cfg_->get_bool(RIGHT_IS_POSITIVE, true);
		up_is_positive = cfg_->get_bool(UP_IS_POSITIVE , true);
		zup_is_positive = cfg_->get_bool(ZUP_IS_POSITIVE , true);
		b_hotfix = cfg_->get_bool("b_hotfix" , false);

		exchange_z = cfg_->get_bool(EXCHANGE_Z, false);

		movemode_direction_x_inverted = cfg_->get_bool(MOVEMODE_DIRECTION_X_INVERTED, false);
		movemode_direction_y_inverted = cfg_->get_bool(MOVEMODE_DIRECTION_Y_INVERTED, false);
		movemode_direction_z_inverted = cfg_->get_bool(MOVEMODE_DIRECTION_Z_INVERTED, false);


		//get port cfg
		port_number_shutter_ = cfg_->get_int(SHUTTER_PORT, 2);
		port_number_stage_ = cfg_->get_int(STAGE_PORT, 1);
		//
		stage_ctl_shutter = cfg_->get_bool(STAGECONTROLSHUTTER, true);
	}

	void set_stage_port_number(int p){
		cfg_->set_int(STAGE_PORT, port_number_stage_ = p);
	};

	int get_basexy(){ return basexy;}
	int get_basez(){ return basez;}
	int get_basevxy(){ return basevxy;}
	int get_basevz(){ return basevz;}

	bool is_right_positive(){return right_is_positive;}
	bool is_up_positive(){return up_is_positive;}
	bool is_zup_positive(){return zup_is_positive;}
	bool is_hotfix_enabled(){return b_hotfix;}
	bool is_z_exchanged(){return exchange_z;}
	bool is_movemode_direction_x_inverted(){
		return movemode_direction_x_inverted;
	}
	bool is_movemode_direction_y_inverted(){
		return movemode_direction_y_inverted;
	}
	bool is_movemode_direction_z_inverted(){
		return movemode_direction_z_inverted;
	}
	int get_stage_port_number(){
		return port_number_stage_;
	};

	void set_shutter_port_number(int p){
		cfg_->set_int(SHUTTER_PORT, port_number_shutter_ = p);
	};

	int get_shutter_port_number(){
		return port_number_shutter_;
	};

	bool is_stage_ctl_shutter(){
		return stage_ctl_shutter;
	};

	void set_stage_ctl_shutter(bool b){
		stage_ctl_shutter = b;
		cfg_->set_bool(STAGECONTROLSHUTTER, b);
	}




private:
	cfg * cfg_;

	int basexy, basez, basevxy, basevz;
	bool right_is_positive, up_is_positive, zup_is_positive;
	bool movemode_direction_x_inverted, 
		movemode_direction_y_inverted, 
		movemode_direction_z_inverted;
	bool exchange_z;
	bool b_hotfix;
	int port_number_stage_, port_number_shutter_;
	bool stage_ctl_shutter;


};






