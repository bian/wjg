﻿#pragma once
#include "deviceInterface.h"
class devices: public deviceInterface
{
public:
  devices(void){};
  ~devices(void){
	  stage.closeCom();
	  shutter.closeCom();
  };

  void SetupShutterInterfaces(bool stage_can_caontrol_shutter);
  bool ShutterOpen(){return shutter_open_func_();}
  bool ShutterClose(){return shutter_close_func_();}
  bool ShutterOpenWithTimeInterval(int ms){return shutter_point_func_(ms);}
	
  bool setup_module();
  	
  void set_percentage_speed(int speed){ percent_spd_axis_xy_ = speed;}
  void set_real_speed(int speed){ exact_spd_axis_xy_ = speed;}
  void set_point_interval(int pI){ point_interval_ = pI;}
 private:
  int percent_spd_axis_xy_, exact_spd_axis_xy_, point_interval_;
  std::function<bool()>shutter_open_func_;
  std::function<bool()>shutter_close_func_;
  std::function<bool(int)>shutter_point_func_;

  template <typename Interface>
  bool SetupInterface(const char* filepath, const char* func_name,
	  Interface& device_interface);
};



