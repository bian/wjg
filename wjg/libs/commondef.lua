command_id = -1
function getid()
	command_id = command_id + 1
	return command_id
end

--[[
-- below interfaces are available commands    
--]]
MoveXY_p = getid()
Z_p = getid()
rZ_p = getid() -- move Z with cord in X(um), with spd in y(4-100%)
rZ_a = getid()
rMoveXY_p = getid()
rMoveXY_a = getid()
SetSpeed_p = getid()
SetSpeed_a = getid()
LaserOn = getid()
LaserOff = getid()
Point = getid()
Wait_ms = getid()
