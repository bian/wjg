﻿#pragma once
#include "stdheader.h"

using namespace std;

extern "C"{
#include "lua/lua.h"  
#include "lua/lualib.h"
#include "lua/lauxlib.h" 
}

/**
* 
*/
class lua_engine
{
	//RAII, keep stack size after operation
	class LuaStackAutoRestore
	{
	public:
		LuaStackAutoRestore( lua_State* vpSL )
		{
			pos_backup_    = lua_gettop( vpSL );
			state_backup_    = vpSL;
		}

		void Restore()
		{
			lua_settop( state_backup_,pos_backup_ );
			state_backup_    = NULL;
		}

		~LuaStackAutoRestore( )
		{
			if( state_backup_ )
				lua_settop( state_backup_,pos_backup_ );
		}

	private:
		lua_State*    state_backup_;;
		int            pos_backup_;
	};
protected:
	lua_State *state_;
public:
	lua_engine()
	{
		::setlocale(LC_ALL,"chs");

		state_=luaL_newstate();
		luaopen_base(state_); 
		luaL_openlibs(state_);
	}
	~lua_engine()
	{
		lua_close(state_);
	}

	lua_State *getinterface()
	{
		return state_;
	}

	void RegFunc(luaL_Reg* list)
	{
		lua_pushglobaltable(state_);
		luaL_setfuncs(state_,list,0);
		lua_pop(state_,1);
	}
	inline bool run_lua_scripts(const char* path)
	{
		LuaStackAutoRestore autoStack(state_);
		return luaL_dofile(state_,path) == 0;
	}
	bool run_string(const char* s, char * err){
		LuaStackAutoRestore autoStack(state_);
		int res = luaL_dostring(state_, s) == 0;
		if (res)
		{
			strcpy(err, lua_tostring(state_, -1));
		}
	}
	inline void get_name(char*name)//ok
	{
		lua_getglobal(state_, "plugin_name");// -0,+1,-1
		strcpy(name, lua_tostring(state_, -1));//获取栈顶元素  -0,+0,-1
		lua_pop(state_, 1);                // 从栈中弹出返回值 ,0
	}
	inline void get_version(char*v)//ok
	{
		lua_getglobal(state_, "version");// -0,+1,-1
		strcpy(v, lua_tostring(state_, -1));//获取栈顶元素  -0,+0,-1
		lua_pop(state_, 1);                // 从栈中弹出返回值 ,0
	}
	inline void get_guielements(std::map<int, string>&key,std::map<int,int>&value)//ok
	{
		{
			LuaStackAutoRestore autoStack(state_);
			lua_getglobal(state_, "guiElements");//-1
			lua_pushnil(state_);//压nil到-1,    -0,+1,-2
			while (lua_next(state_, -2) != 0)//查下一个，-1为value，-2为key,-1
			{
				//-1,+2,-3
				key.insert(pair<int, string>(lua_tointeger(state_,-2), string(lua_tostring(state_, -1))));
				//if(lua_type(state_, -1)==LUA_TNUMBER)
				//  value.push_back(lua_tointeger(state_,-1));//读value
				//else 
				//  value.push_back(lua_toboolean(state_,-1));
				lua_pop(state_, 1);//弹value, -2
			}
		}
		{
			LuaStackAutoRestore autoStack(state_);
			//获取值
			lua_getglobal(state_, "guiValue");//-1
			lua_pushnil(state_);//压nil到-1,    -0,+1,-2
			while (lua_next(state_, -2) != 0)//查下一个，-1为value，-2为key
			{
				value.insert(pair<int,int>(lua_tointeger(state_,-2),lua_tointeger(state_,-1)));//读key 
				lua_pop(state_, 1);//弹value, -2
			}
		}

	}
	inline void set_guielements(std::map<int,int>&value)//ok
	{
		LuaStackAutoRestore autoStack(state_);
		lua_getglobal(state_, "guiValue"); //-0,+1,-1
		//      char tmp[100];
		for(auto i=value.begin();i!=value.end();i++)
		{
			lua_pushnumber(state_, i->second);    //-0,+1,-2
			//wcstombs(tmp,key[i],100);
			lua_rawseti(state_, -2, i->first)  ;  // -1,+0,-1
		}
		lua_pop(state_, 1);//弹value, -2
	}
	inline void start_script()//ok
	{
		LuaStackAutoRestore autoStacker(state_);
		lua_getglobal(state_, "add_elements");//-1
		lua_call(state_, 0, 0);//0
		// lua_pop(state_, 1); //0
	}
	inline void start_debug_scripts()//ok
	{
		LuaStackAutoRestore autoStacker(state_);
		lua_getglobal(state_, "testfunc");//-1
		lua_call(state_, 0, 0);//0
		//lua_pop(state_, 1); 
	}
};
