#pragma once

#include "windows.h"
#include "math.h"
#include "stdlib.h"
//std
#include "string"
#include <fstream>
#include "iostream"
#include "functional"
#include <queue>
#include <cmath>
#include <vector>
#include <map>
#include <cstdlib>//clib mbstowcs
//boost
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>

#include "logger.hpp"
