#include "def.h"
#include "vector"
#include "ago.h"
#include <iostream>
using namespace std;
/**
* get top points
* @param  sln    [description]
* @param  points [size : 6]
* @return        [is absolute coordinate?]
*/
bool getedge(const vector<slnn>& sln, long* points);

int main(int argc, char **argv)
{
	Solution sln;
	if (argc >= 2)
		parseSolution(sln, argv[1]);
	else
		parseSolution(sln);
	long points[6];
	getedge(sln.sln, points);
	cout << " "<< points[0] << " "<< points[1] << " "<< points[2] << " "<< points[3] <<" "<< points[4] << " "<< points[5] << endl;
	return 0;
}

bool getedge(const std::vector<slnn>& sln, long* points){
	long minx = 100000000, miny = 100000000, minz = 100000000;
	long maxx = -100000000, maxy = -100000000, maxz = -100000000;
	long x=0, y=0, z=0;
	bool absolute = false;
	// if 3d solution
	if(sln[0].l == Z_p){
		x = sln[1].x; y = sln[1].y;
		z = sln[0].x;
	}

	for_each(sln.begin(), sln.end(), [&](const slnn& kj){
		switch(kj.l)
		{
			case MoveXY_p:{
				absolute = true;
				x = kj.x;
				y = kj.y;
				break;
			}
			case Point:
			{
				break;
			}
			case rZ_p:
			case rZ_a:
			{
				z += kj.x;
				break;
			}
			//
			case rMoveXY_p:
			case rMoveXY_a:{
				x += kj.x;
				y += kj.y;
				break;
			}
			case Z_p:
			{
				absolute = true;
				z = kj.x;
				break;
			}
            default:
                break;
		}
		if (x>maxx) maxx = x;
		if (y>maxy) maxy = y;
		if (z>maxz) maxz = z;
		if (x<minx) minx = x;
		if (y<miny) miny = y;
		if (z<minz) minz = z;
	} );
	points[0] = minx; points[1] = maxx;
	points[2] = miny; points[3] = maxy;
	points[4] = minz; points[5] = maxz;
	return absolute;
}