
all:
	make -C build
w:
	make -C build weijg
p: gcc
	@-premake4 clean
	@-premake4 gmake
	make -C build
vs2010: c
	@-copy %VSLIBS%\\lib\gcc_dll\\wxmsw294u*.dll bin /y
gcc:c
	@-copy %wxwin%\\lib\gcc_dll\\*.dll bin /y
	
c:
	@-xcopy wjg\\Plugins bin\\Plugins /s /e /i /y
	@-xcopy wjg\\libs bin\\libs /s /e /i /y
	@-xcopy wjg\\res bin\\res /s /e /i /y
	@-xcopy runtime bin /s /e /i /y
	@-xcopy doc bin\\doc /s /e /i /y
clean:
	make -C build clean
	premake4 clean