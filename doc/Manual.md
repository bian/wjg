# 微加工程序手册
> BY BIANHUADONG. Written at 2012-12-22.

## TODO 控制平台和激光快门
![lua plugins](img/shutter.png "")
![lua plugins](img/stage.png "")
## TODO 如何刻写图片 , 如何刻写三维模型
![lua plugins](img/gen1.png "")
## TODO 如何使用lua插件来刻写自定义结构
![lua plugins](img/gen2.png "")
## 如何预览方案
在生成一个加工方案之后，可能需要事先预览加工结果，判断是否符合预期。软件为此也加入了预览功能，使用方法也极其傻瓜。
预览之前需要先生成一个方案。在工具栏会找到一个按钮
![lua plugins](img/previewbt.png "")
点击之后即可立刻预览成品。如下图是加工3个三维管道的预览
![lua plugins](img/preview.png "")
点阵预览
![lua plugins](img/preview2.png "")
#### 小技巧：
3. 鼠标可以绕y轴旋转模型
2. 上下左右键可以移动模型的位置。
1. 按`l`键（表示label）可以开关显示关键的点的坐标。如ls表示线的起点，le表示线的终点。rls，rle为相对的坐标。

![lua plugins](img/preview3.png "")
## 如何写lua插件
weijg支持lua写插件。
> 注意:

> 1.lua源文件编码请设置为带BOM的utf-8。其他编码会不能正确解析，导致插件界面字符乱码。

> 2.lua源文件需要放在Plugins目录才会生效

按照下面的规格写的插件将会显示在程序的插件标签页里，如下图

![lua plugins](img/luaplugin.png "lua plugins")


### 下面简单示例如何编写一个刻写矩形方框的插件。
	-- lua里面，`--`表示注释当前行，不会影响代码。适当的注释有助于代码的阅读
	-- 插件名称，版本信息
	plugin_name ,version = "矩形", "2012.12.21"
	-- 传给程序界面的元素名称
	guiElements={
	[1]="长(左右)/um",
	[2]="宽(上下)/um"
	}
	-- 传给程序界面的对应元素的值
	guiValue={
	[1]=200,
	[2]=100,
	}
	-- 导入指令定义，该文件在程序目录下面的libs文件夹。叫做commondef.lua。有任何第三方lua库也可以放到这个libs文件夹里。指令在数据类型上实际是整数。之所以定义成变量，主要是方便记忆和阅读
	require "libs.commondef"
	-- 生成方案的实现部分，函数名必须是`add_elements`，主程序会自动调用。这里唯一可用的就是add函数。他的作用就是添加一行指令到方案文件中。每一行指令表示一个动作，add(x, y, cmd)，x，y一般表示对应的xy轴坐标，可能是绝对的也可能是相对的，还可以是特定的意义，可以在定义文件commondef.lua里面找到。cmd表示执行的指令，比如相对移动。
	function add_elements()
		local h=guiValue[1]--"长/um"]
		local v=guiValue[2]--"宽/um"]
		add(h,0,rOpenSlow)
		add(0,v,rSlowMove)
		add(-h,0,rSlowMove)
		add(0,-v,rSlowToClose)
	end
### 控制指令介绍
可用指令的导入是用`require "libs.commondef"`来实现的。下面贴出该文件中的指令，以及对应的详细解释。大部分指令的变量名都尽量做到不用注释也能看懂。__指令以r开头的都是相对移动指令，意味着x,y里面的值都是相对当前位置的。若无声明，其他坐标都是指平台上的绝对坐标。__

	fastMoveToOpen = getid()
	-- 快速移动到x,y点，并且打开快门
	fastMoveToPoint=getid()
	-- 快速移动到x,y点，并且打一个点（即开快门等待一段时间，再关闭快门），打点时间默认是200ms
	slowMoveTo=getid()
	-- 使用软件面板定义的速度(%)来慢速移动到x,y。在这之前应该有一个打开快门的指令被调用，否则没意义，不如改用快速移动
	slowMoveToClose=getid()
	-- 使用软件面板定义的速度(%)来慢速移动到x,y，并且关闭快门
	point=getid()
	-- 在当前位置打一个点，打点时间为x，单位ms。y坐标任意
	rZ=getid()
	-- 相对移动 Z 轴x um,使用速度 y%。y范围(4-100%)
	rFastMove=getid()
	-- 相对当前位置移动x,y um
	rSlowMove=getid()
	-- 相对当前位置移动x,y um，速度为软件面板定义
	rLine=getid()
	-- 相对当前位置移动x,y um，但是移动前打开快门，移动后关闭快门。即扫线，故称为line。速度为软件面板定义
	rSlowToClose=getid()
	rOpenSlow=getid()
	rFastMoveToPoint=getid()
	fastMoveToZ=getid()
	-- absolutely move Z with cord in X(um), with constant speed 20%
	Z=getid()
	-- absolutely move Z with cord in X(um), with speed in y(4-100%)
	ex_set_speed=getid()
	-- 设置一个速度值，单位um/s。该值位于内存中固定位置，将用于下面ex开头的指令使用
	exr_line=getid()
	-- 使用上面存入的速度扫线。由于有r在ex前缀后面，所以是相对移动

> 注：getid()是为了模拟c/c++中的enum，来自的生成连贯的数字。



## About
 > All rights reserved. Powered by `Markdown Preview` plugin of Sublime Text 2.