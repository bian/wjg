﻿#include "stdheader.h"
#include "ago.h"
#define BOOST_TEST_MODULE Test_wjg_main_entry
#include <boost/test/unit_test.hpp>
using namespace std;

BOOST_AUTO_TEST_CASE( should_NOT_switch_to_fastmove_mode)
{
	int vx,vy,time;
	swi2fastmove(12,9,3,vx,vy,time);
	BOOST_CHECK_EQUAL( vx,floor(0.5+12/5.));
	BOOST_CHECK_EQUAL( vy,floor(0.5+9/5.));
	BOOST_CHECK_EQUAL( time , 5000);
}

BOOST_AUTO_TEST_CASE( should_switch_to_fastmove_mode)
{
	int vx,vy,time;
	swi2fastmove(12,9,3000,vx,vy,time);
	BOOST_CHECK_EQUAL( time , 5);
}