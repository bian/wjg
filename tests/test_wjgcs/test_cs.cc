#include "def.h"
#include "m.h"
#define BOOST_TEST_MODULE Test_wjg_main_entry
#include <boost/test/unit_test.hpp>
using namespace std;

bool sln_equal(slnn&x, slnn&y)
{
    bool res = (x.x == y.x && x.y == y.y && x.l == y.l);
    if(!res)
        printf("%d %d %d != %d %d %d\n", x.x, x.y, x.l, y.x, y.y, y.l);
    return res;
}

BOOST_AUTO_TEST_CASE( should_get_right_path)
{
    set<Pt> pic_;
    pic_.insert(Pt(1,1));
    pic_.insert(Pt(2,1));
    pic_.insert(Pt(1,2));
    pic_.insert(Pt(2,2));
    pic_.insert(Pt(1,3));
    pic_.insert(Pt(2,3));

    vector<slnn> sln;

    int spd = 13;
    getsolution(pic_, sln, 1, spd, 200);

    /*for(size_t i=0; i!= sln.size(); ++i)
        printf("%d %d %d; ", sln[i].x, sln[i].y, sln[i].l);
    printf("\n");*/
    
    BOOST_CHECK_EQUAL(8, sln.size());
	BOOST_CHECK( sln_equal(sln[0], slnn(100,0,SetSpeed_p)));
	BOOST_CHECK( sln_equal(sln[1], slnn(1,1,MoveXY_p)));
	BOOST_CHECK( sln_equal(sln[2], slnn(0,0,LaserOn)));
	BOOST_CHECK( sln_equal(sln[3], slnn(spd,0,SetSpeed_p)));
	BOOST_CHECK( sln_equal(sln[4], slnn(1,3,MoveXY_p)));
	BOOST_CHECK( sln_equal(sln[5], slnn(2,3,MoveXY_p)));
	BOOST_CHECK( sln_equal(sln[6], slnn(2,1,MoveXY_p)));
	BOOST_CHECK( sln_equal(sln[7], slnn(0,0,LaserOff)));
}

//BOOST_AUTO_TEST_CASE( should_get_right_path_2)
//{
//    set<Pt> pic_;
//    pic_.insert(Pt(1,1));
//    pic_.insert(Pt(2,1));
//    pic_.insert(Pt(1,2));
//    pic_.insert(Pt(2,2));
//    pic_.insert(Pt(1,3));
//    pic_.insert(Pt(2,3));
//
//    vector<slnn> sln;
//
//    int spd = 13;
//    getsolution(pic_, sln, 1, spd, 200);
//
//    /*for(size_t i=0; i!= sln.size(); ++i)
//        printf("%d %d %d; ", sln[i].x, sln[i].y, sln[i].l);
//    printf("\n");*/
//    
//    BOOST_CHECK_EQUAL(8, sln.size());
//	BOOST_CHECK( sln_equal(sln[0], slnn(100,0,SetSpeed_p)));
//	BOOST_CHECK( sln_equal(sln[1], slnn(1,1,MoveXY_p)));
//	BOOST_CHECK( sln_equal(sln[2], slnn(0,0,LaserOn)));
//	BOOST_CHECK( sln_equal(sln[3], slnn(spd,0,SetSpeed_p)));
//	BOOST_CHECK( sln_equal(sln[4], slnn(1,3,MoveXY_p)));
//	BOOST_CHECK( sln_equal(sln[5], slnn(2,3,MoveXY_p)));
//	BOOST_CHECK( sln_equal(sln[6], slnn(2,1,MoveXY_p)));
//	BOOST_CHECK( sln_equal(sln[7], slnn(0,0,LaserOff)));
//}