#pragma once
#include <functional>
using namespace std;
/**
 * 距离均无单位
 */
struct stageInterface
{
    bool (*openCom)(int p);
    void (*closeCom)();

	function<void(long,long)> gr;
	function<void(long,long)> g2;
	function<void(long,long,long)> g3;

    //z
	function<void(long)> zup;
	function<void(long)> zdown;
	function<void(long)> v;
    //
	function<void(int)> sp;
	function<void(int)> spz;

	function<void()> stop;
    //
    function<bool(long&,long&,long&)> getcurpos;
    // get coord
	function<void(long)> px;
	function<void(long)> py;
	function<void(long)> pz;
    //
	function<void(long,long)> vs;
	function<void(long)> vz;

    function<bool()> open_shutter;
    function<bool()> close_shutter;
    function<bool(int)> point;

	function<void()> init_stage;
    function<bool(long)> get_pos_z;
    function<bool()> is_open;
};
