#pragma once
#include <functional>
using namespace std;
struct shutInterface
{
	function<bool(int)> openCom;
	function<void(void)> closeCom;

	function<bool()> openShut;
	function<bool()> closeShut;
	function<bool(int)> openShutInterval;
};