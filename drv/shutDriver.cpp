﻿
#include "serialport.h"
#include "shutter.h"

static timeout_async_serial com;

BOOL APIENTRY DllMain( HANDLE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
	)
{
	switch( ul_reason_for_call )
	{
	case DLL_PROCESS_ATTACH:break;
	case DLL_THREAD_ATTACH:break;
	case DLL_THREAD_DETACH:break;
	case DLL_PROCESS_DETACH:
		com.close();
	}
	return TRUE;
}

extern "C" __declspec(dllexport) void setupShutEngine(shutInterface& dest)
{
  dest.openCom=[&](int p)->bool{return com.open(p);};
  dest.closeCom=[](){ com.close();};

  dest.openShut=[]()->bool{return com.write("@\r");};
  dest.closeShut=[]()->bool{
    return com.write("A\r");
  };
  dest.openShutInterval=[&dest](int ms)->bool{
	if(!dest.openShut()) return false;
	Sleep(ms);
	if(!dest.closeShut()) return false;
	Sleep(20);//等待关闭完全,防止拖线
	return true;
  };
}


