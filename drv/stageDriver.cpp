﻿
#include "serialport.h"
#include "stdio.h"
#include "stdlib.h"
#include "stage.h"
#include "string"


timeout_async_serial com;

const char END_CHAR = '\r';


BOOL APIENTRY DllMain( HANDLE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
    )
{
    switch( ul_reason_for_call )
    {
        case DLL_PROCESS_ATTACH:break;
        case DLL_THREAD_ATTACH:break;
        case DLL_THREAD_DETACH:break;
        case DLL_PROCESS_DETACH:
        com.close();
    }
    return TRUE;
}


void discard_readed()
{
    static char data[255];
    memset(data, 0 ,sizeof(data)/sizeof(char));
    com.read_until(data, END_CHAR);
}

void wait_discard_readed()
{
    com.wait_char();
    discard_readed();
}


bool read(char *buf)
{
    return com.read_until(buf, END_CHAR);
}

void clear_read_buf()
{
	com.clearbuf();
}

bool is_open(){return com.isOpened(); }

auto send_wait_discard = [](const char* command){
    if(!is_open()) return;
    com.write(command);
    wait_discard_readed();
};
auto send_long_wait_discard = [](const char* command, long v){
    if(!is_open()) return;
    char cmd[255];
    sprintf(cmd,"%s %ld\r", command, v);
    com.write(cmd);
    wait_discard_readed();
};
auto send_2long_wait_discard = [](const char* command, long v, long vv){
    if(!is_open()) return;
    char cmd[255];
    sprintf(cmd,"%s %ld,%ld\r", command, v, vv);
    com.write(cmd);
    wait_discard_readed();
};
auto send_3long_wait_discard = [](const char* command, long v, long vv, long vvv){
    if(!is_open()) return;
    char cmd[255];
    sprintf(cmd,"%s %ld,%ld,%ld\r", command, v, vv, vvv);
    com.write(cmd);
    wait_discard_readed();
};
auto send_int_wait_discard = [](const char* command, int v){
    if(!is_open()) return;
    char cmd[255];
    sprintf(cmd,"%s %d\r", command, v);
    com.write(cmd);
    wait_discard_readed();
};

bool openCom(int p)
{
  //int pn = get_right_port("?\r","OPTISCAN");
  return com.open(p);
}

void closeCom(){    com.close();}

extern "C" __declspec(dllexport) void setupStageEngine(stageInterface& dest)
{
    dest.openCom=openCom;
    dest.closeCom=closeCom;

    dest.gr=[](long x,long y) {send_2long_wait_discard("GR", x, y); };
    dest.g2=[](long x,long y) {send_2long_wait_discard("G", x, y); };
    dest.g3=[](long x,long y,long z) {send_3long_wait_discard("GR", x, y, z); };
    //z
    dest.zup=[](long len) {send_long_wait_discard("D", len); };
    dest.zdown= [](long len) {send_long_wait_discard("U", len); };
    dest.v=[](long z) {send_long_wait_discard("V", z); };
    //
    dest.sp=[](int speed) {send_int_wait_discard("SMS", speed); };
    dest.spz=[](int speedz) {send_int_wait_discard("SMZ", speedz); };

    dest.stop=[]() {com.write("K\r"); };
    //
	auto fetchcord_once = [](long& x,long&y,long&z)->bool
	{
		if(!is_open()) return false;

        com.write("P\r");

        com.wait_char();

        static char dat[255];
        memset(dat, 0, sizeof(dat)/sizeof(char));
        if(!read(dat)) return false;

        int res_num = sscanf(dat, "%d,%d,%d", &x, &y, &z);
        if(res_num == 0) return false;
        return true;
	};
    dest.getcurpos=[&](long& x,long&y,long&z)->bool
    {
        long f1[3],f2[3];
		if(fetchcord_once(f1[0],f1[1],f1[2]) && fetchcord_once(f2[0],f2[1],f2[2]))
        {
            if (f1[0]==f2[0] && f1[1]==f2[1] && f1[2]==f2[2])
            {
                x=f1[0];y=f1[1];z=f1[2];
                return true;
            }
        }
        return false;
    };

    dest.px=[](long x) {send_long_wait_discard("PX", x); };
    dest.py=[](long x) {send_long_wait_discard("PY", x); };
    dest.pz=[](long x) {send_long_wait_discard("PZ", x); };
    //
    dest.vs=[](long x,long y) {send_2long_wait_discard("VS", x, y); };
    dest.vz=[](long z) {send_long_wait_discard("VZ", z); };

    dest.open_shutter=[]()->bool
    {
        send_wait_discard("8 A,0\r");
        return true;
    };
    dest.close_shutter=[]()->bool
    {
        send_wait_discard("8 A,1\r");
        return true;
    };
    dest.point=[](int ms)->bool
    {
        send_wait_discard("8 A,0\r");
        Sleep(ms);
        send_wait_discard("8 A,1\r");
        return true;
    };

    dest.init_stage=[]()
    {
        send_wait_discard("COMP 1\r");
    };
    dest.get_pos_z=[](long& z)->bool
    {
        if(!is_open()) return false; 
        com.write("PZ\r");

        static char dat[255];
        memset(dat, 0, sizeof(dat)/sizeof(char));
        if(!read(dat)) return false;
        z=atol(dat);
        return true;
    };
    dest.is_open=is_open;
}


