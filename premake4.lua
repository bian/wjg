-- A solution contains projects, and defines the available configurations
solution "WeijgSolution"
configurations {"Release", "Debug"}
location "./build"
-- linkoptions { "-s" }
language "C++"
defines {"_WIN32_WINNT=0x0501", "WINVER=0x0501", "BOOST_THREAD_USE_LIB"}
includedirs {"$(VSLIBS)/include", "./3rd", "./3rd/lib_bian/include", "$(WXWIN)/include","./drv", "./3rd/lua/include","$(BOOST)/include"}
libdirs {"$(VSLIBS)/lib", "./3rd/lib_bian/lib","./3rd/lua/lib", "$(BOOST)/lib"}
if (_ACTION ~= "vs2010") then
	includedirs { "$(WXWIN)/lib/gcc_dll/mswu" }
	libdirs { "$(WXWIN)/lib/gcc_dll" }
	links {"wsock32", "boost_date_time-mgw47-mt-1_52", "boost_regex-mgw47-mt-1_52", "boost_serialization-mgw47-mt-1_52", "boost_system-mgw47-mt-1_52", "boost_thread-mgw47-mt-1_52", "boost_wserialization-mgw47-mt-1_52"}
	buildoptions { "-std=gnu++11", "-O2", "-fno-strict-aliasing"}

end
if (_ACTION == "vs2010") then
	includedirs {"$(WXWIN)/lib/vc_dll/mswu"}
	libdirs { "$(WXWIN)/lib/vc_dll" }
end
flags {"ExtraWarnings","Unicode"}


project "serialport"
kind "StaticLib"
files {"./3rd/lib_bian/serialport/*.c*"}
targetdir "3rd/lib_bian/lib"

project "stageDriver"
kind "SharedLib"
files {"./drv/stage*.cpp"}
links {"serialport"}
targetdir "bin/"
targetprefix ""
targetextension (".dll")

project "shutDriver"
kind "SharedLib"
files {"./drv/shut*.cpp"}
links {"serialport"}
targetdir "bin/"
targetprefix ""
targetextension (".dll")

-- A project defines one build target
project "weijgcs"
language "C++"
kind "ConsoleApp"
defines { "HAVE_W32API_H","__WXMSW__","NDEBUG","wxNO_RTTI","_UNICODE","NOPCH", "WXUSINGDLL","wxDEBUG_LEVEL=0"}
includedirs { "./wjgcs", "./wjg"}
-- pchheader "wjg/const.h"
links {"wxmsw29u" , "wxtiff" ,"wxexpat" ,"wxpng" ,"wxzlib" ,"wxjpeg" ,"wxregexu","kernel32","user32", "gdi32" ,"comdlg32" ,"winspool" ,"winmm" ,"shell32" ,"comctl32" ,"ole32" ,"oleaut32" ,"uuid" ,"rpcrt4" ,"advapi32" ,"wsock32" ,"odbc32", "logger"}

linkoptions {"-mthreads -W -Wall -fno-rtti  -Wno-ctor-dtor-privacy"}
targetdir "bin"
files {"./wjgcs/*.c*"}

project "solution_info"
language "C++"
kind "ConsoleApp"
includedirs {"./wjg", "./solution_info"}
targetdir "bin"
files {"./solution_info/*.c*", "./wjg/solutionManager.cc"}


project "weijg"
kind "WindowedApp"
includedirs {"./wjg", "./wjg/view"}
-- pchheader "wjg/const.h"
links {"wxmsw29u" , "wxtiff" ,"wxexpat" ,"wxpng" ,"wxzlib" ,"wxjpeg" ,"wxregexu","lua","kernel32","user32", "gdi32" ,"comdlg32" ,"winspool" ,"winmm" ,"shell32" ,"comctl32" ,"ole32" ,"oleaut32" ,"uuid" ,"rpcrt4" ,"advapi32" ,"wsock32" ,"odbc32"}
files {"./wjg/**.cc","./wjg/**.cpp","./wjg/*.rc"}
targetdir "bin"
defines { "HAVE_W32API_H","__WXMSW__","NDEBUG","wxNO_RTTI","_UNICODE","NOPCH", "WXUSINGDLL","wxDEBUG_LEVEL=0" }
linkoptions {"-mthreads -Wl,--subsystem,windows -mwindows -W -Wall -fno-rtti  -Wno-ctor-dtor-privacy"}
flags { "Optimize" }
-- postbuildcommands {}