﻿#pragma once

#include "SketchUpAddin.h"
#include "SketchUp_i.h"

#include<vector>
#include <map>
#include <set>


#ifdef D3CONVERT_EXPORTS
#define ZT_API __declspec(dllexport)
#else
#define ZT_API __declspec(dllimport)
#endif


using namespace std;

typedef  vector<long> db_;

#include "InheritanceManager.h"

ISkpFileReader* pFileReader ;

ISkpApplication* pSkpApp;
CInheritanceManager m_InheritanceManager;

//FILE *fp;

//代表一个点，用于映射需要重载
class Pt
{
public:
	Pt(){x=-1;y=-1;};
	Pt(int x_,int y_){x=(long)x_;y=(long)y_;};
	Pt(long x_,long y_){x=x_;y=y_;};

	bool operator<(const Pt &a) const 
	{
		if(x<a.x)
			return true;
		else if(x==a.x)
			return y<a.y;
		else
			return false;
	};

	long x;
	long y;
};
typedef  set<Pt> mpic;


class piece
{
public:
	piece();
	piece(long &xl,long &yl)
	{
		mimg.insert(Pt(xl,yl));
	};//,long zl);
	~piece()
	{
		mimg.clear();
	};
	mpic mimg;
	//处理结果
	//data_ sx,sy,fg;
	void Add(long x1,long y1)
	{
		mimg.insert(Pt(x1,y1));
	};

};

typedef map<long,piece>pieces;

struct Edge{
	//edge edg;
	db_ sx,sy,sz;
	db_ ex,ey,ez;
	void Add(long x1,long y1,long z1,long x2,long y2,long z2);
	//{
	//	//edg.insert(line(x1,y1,z1,x2,y2,z2));
	//};
	void Exchange2Pieces();
	//void Clear(){edg.clear();};
	void Clear(){sx.clear();sy.clear();sz.clear();ex.clear();ey.clear();ez.clear();};
};

Edge ed;
pieces chain;
typedef void (*paddcord)(long,long ,long );

extern "C"{
	ZT_API void CreateD3Points(BSTR filepath,paddcord padd,long*extremes);//,setPos pos);

}

void WriteFace(ISkpFace* pFace);

void WriteEdge(CComPtr<ISkpEdge> pEdge);

void WriteFacesAndEdges(CComPtr<ISkpEntityProvider> pEntProvider);

void ChainAdd(pieces* dest,long x,long y,long z);
void Write(long x,long y,long z){ChainAdd(&chain,x,y,z);}