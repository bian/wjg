﻿
#include "D3Converter.h"
#include <assert.h> 
#define  ASSERT assert


typedef HRESULT (*GetApplication)(ISkpApplication** ppApplication) ;


void Edge::Add(long x1,long y1,long z1,long x2,long y2,long z2)
{
	for(unsigned int i=0;i!=sx.size();i++)
	{
		if(sx[i]==x1 && sy[i]==y1 && sz[i]==z1)
			if(ez[i]==z2 && ex[i]==x2 && ey[i]==y2)
				return ;
	}

	sx.push_back(x1);sy.push_back(y1);sz.push_back(z1);
	ex.push_back(x2);ey.push_back(y2);ez.push_back(z2);

}

void Edge::Exchange2Pieces(){
	for(unsigned int i=0;i!=sx.size();i++)
	{
		long x1=sx[i],y1=sy[i],z1=sz[i] ,x2=ex[i],y2=ey[i],z2=ez[i];
		long m=x1-x2,n=y1-y2,p=z1-z2;

		if (p == 0)
		{
			if(m!=0 && n!=0){
				// TODO :try to add more points
				Write(x1,y1,z1);
				Write(x2,y2,z2);
			}
			else if (m!=0 && n==0)
			{
				long stp = min(x1,x2), edp = max(x1,x2);
				for(long j = stp;j<=edp;j++)
					Write(j, y1, z1);
			}
			else if (m==0 && n!=0)
			{
				long stp = min(y1,y2), edp = max(y1,y2);
				for(long j = stp;j<=edp;j++)
					Write(x1, j, z1);
			}
		}
		else{
			// TODO : add more points
			if(m==0 && n==0){
				long stp = min(z1,z2), edp = max(z1,z2);
				for(long j = stp;j<=edp;j++)
					Write(x1, y1, j);
			}
			else
			{
				Write(x1,y1,z1);
				Write(x2,y2,z2);
			}
		}

		// if(m!=0){
		// 	if(x1>x2)
		// 		for(long j = x2;j<=x1;j++)
		// 			Write(j,n*(j-x2)/m+y2,p*(j-x2)+z2);
		// 	else
		// 		for(long j = x1;j<=x2;j++)
		// 			Write(j,n*(j-x2)/m+y2,p*(j-x2)+z2);

		// }
		// else if(n!=0){
		// 	if(y1>y2)
		// 		for(long j = y2;j<=y1;j++)
		// 			Write(m*(j-y1)/n+x1,j,p*(j-y1)/n+z1);
		// 	else
		// 		for(long j = y1;j<=y2;j++)
		// 			Write(m*(j-y1)/n+x1,j,p*(j-y1)/n+z1);
		// }
		// else if(p!=0){
		// 	if(z1>z2)
		// 		for(long j = z2;j<=z1;j++)
		// 			Write(m*(j-z1)/p+x1,n*(j-z1)/p+y1,j);
		// 	else
		// 		for(long j = z1;j<=z2;j++)
		// 			Write(m*(j-z1)/p+x1,n*(j-z1)/p+y1,j);
		// }
		// else{
		// 	Write(x1,y1,z1);
		// }


	}
}



//////////////////////////////////////////////////////////////

void WriteFace(ISkpFace* pFace)
{
    HRESULT hr;
    CComPtr<ISkpPolygonMesh> pMesh;

    // This call will create the mesh including 
    // - all the points on the mesh
    // - The UV coordinates for the front face
    // - The UV coordinates for the back face
    // - The normal vectors at each mesh point.
    hr = pFace->CreateMesh(PolygonMeshPoints | PolygonMeshUVQFront | PolygonMeshUVQBack | PolygonMeshNormals, NULL, &pMesh);

    // Now as an example, we iterate through the resulting mesh,
    // printing out the mesh points.
    long numPoints = 0;
	hr = pMesh->get_NumPoints(&numPoints);
    long index;
    for( index = 1; index <= numPoints; index++ )
    {
      double p3[3];

      hr = pMesh->_GetPoint(index, p3);

	  Write((long)p3[0], (long)p3[1], (long)p3[2]);
    }
}


void WriteEdge(CComPtr<ISkpEdge> pEdge)
{
	HRESULT hr;

	CComPtr<ISkpPoint3d> pStartPoint;
	hr = pEdge->get_StartPoint(&pStartPoint);
	CPoint3d startPt(pStartPoint);
	CPoint3d startPoint = m_InheritanceManager.GetCurrentTransform() * startPt;
	//TRACE("<Start x=\"%f\" y=\"%f\" z=\"%f\" />\n", startPoint.X(), startPoint.Y(), startPoint.Z());

	CComPtr<ISkpPoint3d> pEndPoint;
	hr = pEdge->get_EndPoint(&pEndPoint);
	CPoint3d endPt(pEndPoint);
	CPoint3d endPoint = m_InheritanceManager.GetCurrentTransform() * endPt;
	// TRACE("<End x=\"%f\" y=\"%f\" z=\"%f\" />\n", endPoint.X(), endPoint.Y(), endPoint.Z());

	ed.Add((long)(startPoint.X()), (long)(startPoint.Y()), (long)(startPoint.Z()), (long)(endPoint.X()), (long)(endPoint.Y()), (long)(endPoint.Z()));

}


void WriteFacesAndEdges(CComPtr<ISkpEntityProvider> pEntProvider)
{
	HRESULT hr;
	long nElements, i;

	//Write all the faces
	CComPtr<ISkpFaces> pFaces = NULL;
	hr = pEntProvider->get_Faces(&pFaces);
	hr = pFaces->get_Count(&nElements);

	for(i=0; i<nElements; i++)
	{
		CComPtr<ISkpFace> pFace;
		hr = pFaces->get_Item(i, &pFace);

		if (hr==S_OK)
		{
			WriteFace(pFace);
		}
	}

	//Write all the edges
	CComPtr<ISkpEdges> pEdges = NULL;
	hr = pEntProvider->get_Edges(&pEdges);
	hr = pEdges->get_Count(&nElements);

	for(i=0; i<nElements; i++)
	{
		CComPtr<ISkpEdge> pEdge;
		hr = pEdges->get_Item(i, &pEdge);

		if (hr==S_OK)
		{
			WriteEdge(pEdge);
		}
	}
}


void CreateD3Points(BSTR filepath,paddcord padd,long*extremes)//,setPos pos)
{
	pFileReader = 0;

	auto hModule = ::LoadLibraryA("SketchUpReader.dll");

	if(hModule == NULL)  
	{ 
		MessageBoxA(0,"Load SketchUpReader.dll failed","error",	0);
		return;
	}

	GetApplication getApplication = (GetApplication)GetProcAddress(hModule, "GetSketchUpSkpApplication");

	if(getApplication == NULL)   ASSERT(false);

	(*getApplication)(&pSkpApp);

	HRESULT hr = pSkpApp->QueryInterface(&pFileReader);
	ASSERT( hr  == S_OK );

	ISkpDocument* m_pDocument=0;
	hr  =pFileReader->OpenFile(filepath,&m_pDocument);

	if(hr==S_OK)
	{
		CComPtr<ISkpEntityProvider> pEntProvider;
		m_pDocument->QueryInterface(IID_ISkpEntityProvider, (void **)&pEntProvider);

		WriteFacesAndEdges(pEntProvider);
		//change line 2 points
		ed.Exchange2Pieces();
		for(auto i=chain.begin();i!=chain.end();i++)
		{
			for(auto j=i->second.mimg.begin();\
				j!=i->second.mimg.end();j++)
			{
				(*padd)((long)(j->x),(long)(j->y),(long)(i->first));
			}
		}
	}

	ISkpThumbnailProvider * pthumb = NULL;
	hr = m_pDocument->QueryInterface(IID_ISkpThumbnailProvider, (void**)&pthumb);
	ASSERT( hr  == S_OK );

	long iw,ih;
	hr = pthumb->GetThumbnailSize(&iw,&ih);
	BSTR ppp =SysAllocString(_T("d3temp.jpg"));
	pthumb->SaveThumbnail(ppp);
	pthumb->Release();
	SysFreeString(ppp);

	::FreeLibrary(hModule);

}


void ChainAdd(pieces* dest,long x,long y,long z){

	auto iter=dest->end();
	auto fiter=dest->find(z);
	if(fiter==iter)
	{
		//没有这个平面
		dest->insert(pair<long,piece>(z,piece(x,y)));
	}
	else
	{
		//已经存在
		fiter->second.Add(x,y);
	}
}
