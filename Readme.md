微加工项目开发指南
-----

>软件使用方法请[点我](https://bitbucket.org/bian/wjg/wiki/Home)

## 如何下载源代码
* 安装git for win
* clone最新的源代码，`git clone --depth 1 git@bitbucket.org:bian/wjg.git`
* 项目中包含了若干git子项目，需要事先配置好。右键项目文件夹菜单中，选择git bash，运行以下命令进行配置:
 `git submodule init`, `git submodule update`

## 编译准备
### 需求
* 界面库：wxwidgets2.9.4
* c++库：boost 5.2.0
* Lua5.2

编译以上库需要耗费大量时间。为了快速搭建开发环境，我已经全部编译并且打包到一个文件夹里。[点我下载](http://pan.baidu.com/share/link?shareid=3432297718&uk=2617453131)。解压后双击运行里面的脚本(需要win7+)即可自动设置好环境变量。（xp及以下windows系统请手工设置环境变量，方法请百度谷歌。）__环境变量设好之后需要注销或重启才会生效。__

### 开始使用vs2010编译
为了方便编译，特将之前的mingw编译迁移到Visual Studio 2010 sp1编译。低版本的vs将无法编译通过，因为代码中部分使用了c++11特性。

* vs工程文件在vs2010目录，运行weijgSolution.sln。右键项目树的根，切换到release方案，选择build，即可自动编译全部项目。
* 编译之后还需要拷贝必要的运行库到bin目录才能运行。
	* 将位于vs2010_lib中的wxwidgets的dll全部拷过来。
	* 将runtime目录的所有dll拷贝过来。
	以上任务可以通过双击项目根目录的vc_runtime_cp.bat脚本来自动完成。
	* 如需部署到其他机器，请将vs的c++的2个运行库拷贝到bin目录。

以下为其他编译方法，可略过
---------------------------------
### 使用mingw编译（不推荐）（因D3convert中使用了ATL库，还是需要vs才能编译。否则无法使用3d加工功能）
* 编译器：mingw(使用tdm版本，去其官网下载，否则上面用到的库可能无法使用。因为上面的库都是我用tdm编译的)
* 使用cmd，项目根目录运行`premake4 gmake`
* 开始编译，运行`make p`
* bin 目录即为编译好的程序目录