#pragma once

#include "stdheader.h"
#include "def.h"

using namespace std;

inline void swi2fastmove(const long &xr, const long &yr,
 const int &speed, int& vx, int& vy, int& time)
{
  long r;
  double rdb = sqrt(static_cast<double>(xr*xr + yr*yr));
  r = static_cast<long>(rdb);
  vx = static_cast<int>(floor(0.5+ (xr*speed*1.0)/r));
  vy = static_cast<int>(floor(0.5+ (yr*speed*1.0)/r));
  time=1000*r/speed;
};

auto split_str = [](const std::string& str, vector<string>& vStr)
{
  vStr.clear();
  boost::split( vStr, str, boost::is_any_of( "; " ), boost::token_compress_on );
};

inline bool parseSolution(Solution& solution, const char *path = ".\\sln.txt"){
	vector<slnn>& sln = solution.sln;

  ifstream ifs(path);
  const char* flag="weijg sln v4";
  string buf;
  vector<string> ar;
  size_t sz;
  slnn snn;
  auto trim_str = [](string& trimed_str)
    {
      boost::erase_all(trimed_str, "\n");
      boost::erase_all(trimed_str, "\r");
      //boost::trim(trimed_str);
    };
	
  if (ifs.bad()) {
    cout<< "file open failed."<<endl;
    return false;
  }


  std::getline(ifs, buf);
  trim_str(buf);
  if(buf != flag){
    cout << "head is:" << buf <<endl;
    cout<< "file head not valid."<<endl;
    return false; 
  }
	
  //lg<<"parsed scanning speed is "<<spd<<" point time interval is "<<pi <<endl;
  sln.clear();
  while(ifs.good())
    {
      std::getline(ifs, buf);
      trim_str(buf);
      split_str(buf, ar);
      sz=ar.size();
      if(sz!=3)
        {
          if(buf.size()==0) continue;
          cout<< "command is not valid."<<endl;
          return false;
        }
      snn.set(boost::lexical_cast<long>(ar[0]), boost::lexical_cast<long>(ar[1]), boost::lexical_cast<int>(ar[2]));
      sln.push_back(snn);
    }
  ifs.close();


  return true;
};
